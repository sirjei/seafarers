Template.header.onCreated(function () {
  //console.log("CREATED");
});


Template.header.onDestroyed(function () {
  //console.log("DESTROYED");
});

Template.header.events({
  'click .btn-menu': function (e) {
    e.preventDefault();
    $('#sidebar-main').sidebar('toggle');
  },
  'click .btn-profile': function (e) {
    e.preventDefault();
    $('#sidebar-account')
      .sidebar('setting', 'transition', 'overlay')
      .sidebar('toggle');
  },
  'click .btn-bookmarks': function (e) {
    e.preventDefault();
    $('#sidebar-misc')
      .sidebar('setting', 'transition', 'overlay')
      .sidebar('toggle');
  },
  'click .btn-login': function (e) {
    e.preventDefault();
    $('#modal-credentials')
      .modal('setting', 'closable', false)
      .modal('show');
  },
});
