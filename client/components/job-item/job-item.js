Template.jobItem.helpers({
    lastUpdate: function () {
        return this.updatedAt || this.createdAt;
    },
    salaryRange: function() {
        var salary = this.salary;

        if (salary && salary.max && salary.min)
            return '$ ' + salary.min + ' - ' + '$ ' + salary.max;
        else
            return '(To be evaluated based on experience)';
    },
    jobLink: function() {
        return FlowRouter.path('jobDetails', {
            jobId: this._id
        });
    }
});
