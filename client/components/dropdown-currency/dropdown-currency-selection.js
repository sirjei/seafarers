var CurrencyResource = Seafarers.Resources.Currencies;

Template.dropdownCurrencySelection.onRendered(function () {
  $('input[name=currency]').parents('.dropdown').dropdown();
});

Template.dropdownCurrencySelection.helpers({
  currencies: function () {
    return CurrencyResource.getList();
  }
});
