var VesselResource = Seafarers.Resources.Vessels;

Template.dropdownVesselsSelection.onRendered(function () {
  $('input[name=vesselType]').parents('.dropdown').dropdown();
});

Template.dropdownVesselsSelection.helpers({
  vessels: function () {
    return VesselResource.getList();
  }
});
