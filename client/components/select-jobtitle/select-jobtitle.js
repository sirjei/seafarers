var JobTitleResource = Seafarers.Resources.JobTitles;

Template.selectJobTitle.helpers({
  jobTitles: function () {
    return JobTitleResource.getList();
  }
});
