Template.sidebarAccount.events({
  'click .btn-logout': function (e) {
    e.preventDefault();
    $('#sidebar-account').sidebar("hide");
    Meteor.logout();
  }
});

Template.sidebarAccount.onRendered(function () {

});
