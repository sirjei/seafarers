var VesselResource = Seafarers.Resources.Vessels;

Template.selectVessels.helpers({
  vessels: function () {
    return VesselResource.getList();
  }
});
