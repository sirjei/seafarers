var CountryResource = Seafarers.Resources.Countries;

Template.dropdownCountrySelection.onRendered(function () {
  $('input[name=country]').parents('.dropdown').dropdown();
});

Template.dropdownCountrySelection.helpers({
  countries: function () {
    return CountryResource.getList();
  }
});
