var StateResource = Seafarers.Resources.States;

Template.dropdownStateSelection.onRendered(function () {
  $('input[name=state]').parents('.dropdown').dropdown();
});

Template.dropdownStateSelection.helpers({
  states: function () {
    return StateResource.getList();
  }
});
