var JobTitleResource = Seafarers.Resources.JobTitles;

Template.dropdownJobTitleSelection.onRendered(function () {
  $('input[name=jobTitle]').parents('.dropdown').dropdown();
});

Template.dropdownJobTitleSelection.helpers({
  jobTitles: function () {
    return JobTitleResource.getList();
  }
});
