Template.jobDetailsEmailLogin.onCreated(function() {
    var template = this,
    schema = new SimpleSchema(Seafarers.Schemas.Login),
    form, credentials;

    template.schema = schema;
    template.context = schema.newContext();
    template.getCredentials = getCredentials;

    function getCredentials() {
        form = $('#emailLogin');

        credentials = {
            email: form.find('[name=email]').val(),
            password: form.find('[name=password]').val()
        };

        return schema.clean(credentials);
    }
});

Template.jobDetailsEmailLogin.onRendered(function () {
    $('#application-modal').modal('setting', 'onHidden', function () {
        var applicationModal =  Seafarers.Controllers.JobApplication
            .applicationModal;

        if (applicationModal() === 'jobDetailsEmailLogin')
            Seafarers.Controllers.JobApplication
                .applicationModal('jobDetailsLoginOptions');
    });
});

Template.jobDetailsEmailLogin.helpers({
    errClass: function (fieldName) {
        return Template.instance().context.keyIsInvalid(fieldName) ?
            'error' : null;
    },
    errMsg: function (fieldName) {
        return Template.instance().context.keyErrorMessage(fieldName);
    }
});

Template.jobDetailsEmailLogin.events({
    'change input': function (event, template) {
        var context = template.context,
        field = $(event.target).prop('name'),
        credentials = template.getCredentials(),
        email;

        context.validateOne(credentials, field);

        if (field === 'email') {
            email = credentials['email'];

            Meteor.call('checkEmail', email, function (error, result) {
                if (result)
                    Meteor.call('getAccountType', email, function (
                        error, result
                    ) {
                        if (result && result === 'company')
                            context.addInvalidKeys([{name: 'email',
                                type: 'userIsCompany'}]);
                    });
                else
                    context.addInvalidKeys([{name: 'email',
                        type: 'emailNotFound'}]);
            });
        }
    },
    'click #loginWithEmail, submit form': function (event, template) {
        event.preventDefault();

        var credentials = template.getCredentials(),
        context = template.context,
        email = credentials['email'],
        currentJob, jobApplications, isJobApplied, application;

        context.validate(template.getCredentials());

        // do other server validations if email passed the client validation
        if (!_.find(context.invalidKeys(), function (element) {
            return element.name === 'email';
        }))
            Meteor.call('checkEmail', email, function (error, result) {
                if (result)
                    Meteor.call('getAccountType', email, function (
                        error, result
                    ) {
                        if (result && result === 'company')
                            context.addInvalidKeys([{name: 'email',
                                type: 'userIsCompany'}]);
                    });
                else
                    context.addInvalidKeys([{name: 'email',
                        type: 'emailNotFound'}]);
            });

        if (context.invalidKeys().length === 0) {
            Meteor.loginWithPassword(email, credentials.password, function (error) {
                if (!error)
                    Seafarers.Controllers.JobApplication
                        .applicationModal('jobDetailsAfterLogin');
                else if (error.reason === "Incorrect password")
                    context.addInvalidKeys([{name: 'password',
                        type: 'incorrectPassword'}]);
                else
                    toastr.error('Sorry, server error, please try again later.', 'Server Error!');
            });
        }
        else
            toastr.error('Please fill all fields with valid values.', 'Invalid!');
    }
});
