Template.jobDetailsAfterLogin.onCreated(function() {
    this.Steps = Seafarers.Controllers.JobApplication.Steps;
    this.firstStepLink = FlowRouter.path('jobApplicationStep', {
		jobId: FlowRouter.current().params.jobId,
		step: Template.instance().Steps.firstStep
	});
    this.lastStepLink = FlowRouter.path('jobApplicationStep', {
		jobId: FlowRouter.current().params.jobId,
		step: Template.instance().Steps.lastStep
	});
});


Template.jobDetailsAfterLogin.events({
	'click #send-application': function () {
		Meteor.call('sendApplication', FlowRouter.getParam('jobId'),
			function (err, res) {
				if (!err && res) {
					Seafarers.Controllers.JobApplication
						.applicationModal('jobDetailsSuccess');
				}
			});
	},
	'click .close': function (event, template) {
		event.preventDefault();

		$('#application-modal').modal('setting', 'onHidden', function () {
			var link = $(event.target).attr('href');

			if (link && link !== '#')
				FlowRouter.go(link);
		}).modal('hide');
	}
});

Template.jobDetailsAfterLogin.helpers({
	updateCvLink: function () {
		return Template.instance().firstStepLink;
	},
	reviewCvLink: function () {
		return Template.instance().lastStepLink;
	},
	isJobApplied: function () {
        var currentJob = Seafarers.Collections.Jobs
            .findOne(FlowRouter.getParam('jobId')),
        jobApplications = currentJob && currentJob.applications,
        application = jobApplications && _.find(jobApplications, function (e) {
            // find for jobApplication of currentUser
            return e.userId === Meteor.userId();
        });

        if (application && application.isSent)
            return true;
        else
            return false;
	}
});


Template.jobDetailsAfterLogin.events({
    'click #review-cv':function (event, template) {
    	event.preventDefault();
    	template.clickedLink.set(template.lastStepLink);
        $('#application-modal').modal('hide');
    },
    'click #update-cv':function (event, template) {
    	event.preventDefault();
    	template.clickedLink.set(template.firstStepLink);
        $('#application-modal').modal('hide');
    }
});