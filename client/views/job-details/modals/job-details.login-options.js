Template.jobDetailsLoginOptions.helpers({
    applicationLink: function () {
        return FlowRouter.path('jobApplicationStep', {
            jobId: FlowRouter.getParam('jobId'),
            step: Seafarers.Controllers.JobApplication.Steps.firstStep
        });
    }
});

Template.jobDetailsLoginOptions.events({
    'click #emailLogin': function () {
    	Seafarers.Controllers.JobApplication
    		.applicationModal('jobDetailsEmailLogin');
    },
    'click .close': function () {
        $('#application-modal').modal('hide');
    }

});
