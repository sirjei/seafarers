Template.jobDetailsResumeCV.onCreated(function () {
    Meteor.subscribe( // return subscription handle
	    'specificApplicant',
	    {userId: Meteor.userId()});
});

Template.jobDetailsResumeCV.helpers({
    applicationLink: function () {
		var applicantData = Seafarers.Collections.Applicants
			.findOne({userId: Meteor.userId()});

		if (applicantData)
	        return FlowRouter.path('jobApplicationStep', {
	            jobId: FlowRouter.getParam('jobId'),
	            step: applicantData.lastStep
	        });
    }
});

Template.jobDetailsResumeCV.events({
	'click .close': function () {
		$('#application-modal').modal('hide');
	}
});