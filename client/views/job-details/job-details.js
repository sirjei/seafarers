Template.jobDetails.onCreated(function() {
    this.currentJob = new ReactiveVar();
    this.salaryRange = new ReactiveVar();
    this.wasLoggedIn = new ReactiveVar();
});

Template.jobDetails.onRendered(function() {
    var template = this,
    isDataInitiated = false,
    currentJob = template.currentJob.get(),
    db = Seafarers.Collections,
    companyData, jobData, salary;

    template.autorun(function() {
        companyData = db.Companies.findOne();
        jobData = db.Jobs.findOne();

        if (companyData && jobData && !isDataInitiated) {
            salary = jobData.salary;

            template.currentJob.set(_.extend(
                companyData,
                jobData
            ));

            if (salary && salary.max && salary.min)
                template.salaryRange
                    .set('$ ' + salary.min + ' - ' + '$ ' + salary.max);
            else
                template.salaryRange
                    .set('(To be evaluated based on experience)');

            isDataInitiated = true;
        }

        if (Meteor.user())
            template.wasLoggedIn.set(true);
    }); // end of autorun
});

Template.jobDetails.onDestroyed(function () {
    $('#application-modal').remove();
});

Template.jobDetails.helpers({
    applicationModalTemplate: function () {
        var applicationModal = Seafarers.Controllers.JobApplication
            .applicationModal,
        wasLoggedIn = Template.instance().wasLoggedIn;

        // rerun on log out
        if (wasLoggedIn.get() && !Meteor.user()){
            wasLoggedIn.set(false);
            applicationModal('jobDetailsLoginOptions');
        }

        return applicationModal();
    },
    currentJob: function () {
        return Template.instance().currentJob.get();
    },
    salaryRange: function () {
        return Template.instance().salaryRange.get();
    },
    sidebarData: function () {
        var template = Template.instance(),
        currentJob = Seafarers.Collections.Jobs
            .findOne(FlowRouter.getParam('jobId')),
        jobApplications = currentJob && currentJob.applications,
        isJobApplied, application;

        if (jobApplications) {
            application = _.find(jobApplications, function (e) {
                // find for jobApplication of currentUser
                return e.userId === Meteor.userId();
            });

            isJobApplied = application && application.isSent;
        }

        return {
            salaryRange: template.salaryRange.get(),
            isJobApplied: isJobApplied
        }
    }
});

Template.jobDetails.events({
    'click #openModal': function (event,template) {
        $('#application-modal').modal('show');
    }
});