Template.jobDetailsSidebar.onCreated(function () {
	this.Steps = Seafarers.Controllers.JobApplication.Steps;
});

Template.jobDetailsSidebar.helpers({
	updateCvLink: function () {
		return FlowRouter.path('jobApplicationStep', {
			jobId: FlowRouter.getParam('jobId'),
			step: Template.instance().Steps.firstStep
		});
	},
	reviewCvLink: function () {
		return FlowRouter.path('jobApplicationStep', {
			jobId: FlowRouter.getParam('jobId'),
			step: Template.instance().Steps.lastStep
		});
	}
});