Template.jobEditBreadcrumbs.helpers({
    stepDisplayName: function () {
        var currentStep = FlowRouter.getParam('step');
        return currentStep && Seafarers.Controllers.JobEdit.Steps
        	.getOne(currentStep).displayName;
    }
});
