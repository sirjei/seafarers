Template.jobEditJobDetails.onCreated(function() {
    var template = this,
    schema = Seafarers.Schemas.JobDetails,
    currentUser = Meteor.user(),
    userId = currentUser._id,
    controller = Seafarers.Controllers.JobEdit,
    form, userInfo, data;

    template.Steps = controller.Steps;
    template.currentUser = new ReactiveVar(currentUser);
    template.schema = schema;
    template.context = schema.newContext();
    template.getFormData = getFormData;
    template.isReady = new ReactiveVar(false);
    template.isSalaryDisabled = new ReactiveVar('disabled');

    Seafarers.Collections.Jobs.attachSchema(Seafarers.Schemas.DBJobDetails, {replace: true});

    controller.subscribe(template, currentUser._id);

    function getFormData(schema) {
    	form = template.$('#jobDetails');
        userInfo = {};
    	data = {
    		title: form.find('[name=title]').val(),
    		vesselType: form.find('[name=vesselType]').val(),
    		description: form.find('[name=description]').val(),
    		hasSalary: form.find('[name=hasSalary]:checked').val()
    	};

        if (data.hasSalary === 'true') {
            data.hasSalary = true;
            data.salary = {
                min: form.find('[name=salary-min]').val(),
                max: form.find('[name=salary-max]').val()
            };
        } else
            data.hasSalary = false;

        schema.clean(data);

    	return data;
    }
});

Template.jobEditJobDetails.onRendered(function() {
    var template = this,
    currentUser = template.currentUser.get(),
    Steps = template.Steps,
    userId = currentUser._id,
    jobId = FlowRouter.getParam('jobId'),
    Jobs = Seafarers.Collections.Jobs,
    currentStep = FlowRouter.getParam('step'),
    companyData, currentJob;

    template.$('[name=salary-min]').numberInput().maxInputLength(7);
    template.$('[name=salary-max]').numberInput().maxInputLength(7);

    template.autorun(function() {
        companyData = Seafarers.Collections.Companies.findOne({userId: userId});
        currentJob = Jobs.findOne({authorId: userId});

        // if unverified account is trying to create new job (again),
        // give him the draft previously created
        if (companyData && !currentUser.emails[0].verified && jobId === 'new' && currentJob) {
            jobId = currentJob._id;
            FlowRouter.go('jobEditStep', {
                jobId: currentJob._id,
                step: FlowRouter.current().params.step
            });
        }

        if (companyData && currentJob && !template.isReady.get()) {
            if (currentJob.hasSalary) {
                $('[name=hasSalary][value=true]').prop('checked', true);
                template.isSalaryDisabled.set('');
            }

            _.each([
                {field: 'employmentBenefits', Field: 'Benefits'},
                {field: 'requiredTrainings', Field: 'Trainings'},
                {field: 'otherQualifications', Field: 'Qualifications'}
            ], function (element) {
                initStrArrInput({
                    currentJob: currentJob,
                    Jobs: Jobs,
                    template: template,
                    field: element.field,
                    Field: element.Field,
                });
            });

            Steps.saveLastStep(currentStep, companyData);
            Steps.check();

            if (!Steps.getOne(FlowRouter.current().params.step).isEnabled)
                FlowRouter.go('accessDenied');

            template.isReady.set(true);
        }
    }); // end of autorun

    function initStrArrInput(arg) {
        var currentJob = arg.currentJob,
        Jobs = arg.Jobs,
        template = arg.template,
        field = arg.field,
        Field = arg.Field,
        strings = [],
        jobId = currentJob._id,
        modifier, data;

        if (currentJob[field])
            strings = currentJob[field];
        else { // insert empty array of strings if there's none
            modifier = {};

            modifier[field] = [];

            Jobs.update({
                _id: jobId
            }, {
                $set: modifier
            }, function (error, affected) {
                if (error)
                    throw error;
            });
        }

        template[Field] = new Seafarers.Core.Factories.StrArrInput({
            strings: strings,
            collection: Jobs,
            documentId: jobId,
            field: field
        });
    }

    $('#next-step').click(function (e) {
        e.preventDefault();

        data = template.getFormData(template.schema);

        if (template.context.validate(data)) {
            FlowRouter.go('jobEditStep', {
                jobId: FlowRouter.getParam('jobId'),
                step: Steps.getNext(currentStep).name
            });
        } else
            toastr.error('Please fill form with valid data', 'Invalid!');
    });
});

Template.jobEditJobDetails.onDestroyed(function () {
    $('#next-step').unbind('click');
});

Template.jobEditJobDetails.helpers({
    currentJob: function () {
        return Seafarers.Collections.Jobs.findOne({authorId: Meteor.userId()});
    },
    errClass: function (fieldName) {
        return Template.instance().context.keyIsInvalid(fieldName) ? 'error' : null;
    },
    errMsg: function (fieldName) {
        return Template.instance().context.keyErrorMessage(fieldName);
    },
    disableSalary: function () {
        return Template.instance().isSalaryDisabled.get();
    },
    strArrFields: function () {
        var template = Template.instance();

        if (template.isReady.get())
            return [
                {
                    long: 'EMPLOYMENT BENEFITS',
                    short: 'benefits',
                    strings: template['Benefits'].get()
                },
                {
                    long: 'REQUIRED TRAININGS',
                    short: 'trainings',
                    strings: template['Trainings'].get()
                },
                {
                    long: 'OTHER QUALIFICATIONS',
                    short: 'qualifications',
                    strings: template['Qualifications'].get()
                }
            ];
    }
});

Template.jobEditJobDetails.events({
    'click label.salary': function (event, template) {
        var $input = $(event.target).prev(),
        isChecked = $input.prop('checked'),
        isDisabled;

        if ($input.val() !== 'true') {
            isDisabled = 'disabled';
            template.$('[name=salary-min]').val('');
            template.$('[name=salary-max]').val('');
        }

        $input.prop('checked', !isChecked);
        Template.instance().isSalaryDisabled.set(isDisabled);
    },
    'change [name=hasSalary]': function (event, template) {
        var $input = $(event.target),
        isDisabled;

        if ($input.val() !== 'true') {
            isDisabled = 'disabled';
            template.$('[name=salary-min]').val('');
            template.$('[name=salary-max]').val('');
        }

        Template.instance().isSalaryDisabled.set(isDisabled);
    },
    'change .add-string': function (event, template) {
        if (template.isReady.get()) {
            $input = $(event.target);

            template[this.short.capitalize()].add($input.val());
            $input.val('');
        }
    },
    'click .remove-string': function (event, template) {
        event.preventDefault();

        var $this = $(event.target),
        name = $this.parents('.string-array-set').prop('id').capitalize();

        if (template.isReady.get())
            template[name].remove($.trim($this.parents('.custom').text()));
    },
    'change input:not(.add-string), change select, change textarea, click label.salary': function (event, template) {
        if (template.isReady.get()) {
            var context = template.context,
    		field = $(event.target).prop('name') || 'hasSalary',
            Steps = template.Steps,
            Jobs = Seafarers.Collections.Jobs,
            data = template.getFormData(template.schema),
            permitUpdate = true,
            newData = {
                // updatedAt: new Date()
            }, newContext;

            Seafarers.Helpers.Forms.validateEachOther({
                inputOne: 'min',
                inputTwo: 'max',
                current: field,
                context: context,
                data: data
            });

            if (field.indexOf('-'))
                field = field.replace('-', '.');


            if (field.indexOf('alary') !== -1) { // if it is any salary field,
                if (data.hasSalary === true) {
                    newContext = template.schema.newContext();
                    if( // and one of salary field is invalid,
                        !newContext.validateOne(data, 'salary.min') ||
                        !newContext.validateOne(data, 'salary.max')
                    )
                        permitUpdate = false;
                    else { // if valid, pass the whole salary object and hasSalary field
                        newData.salary = data.salary;
                        newData.hasSalary = data.hasSalary;
                    }
                } else { // else if its hasSalary and is false, delete the salary object
                    context.validateOne(data, 'salary.min');
                    context.validateOne(data, 'salary.max');

                    newData = {
                        'hasSalary': false,
                        'salary': ''
                    };
                }
            } else
                newData[field] = _.isUndefined(data[field]) ? '' : data[field];

            if (context.validateOne(data, field) && permitUpdate) {
                Jobs.update(Jobs.findOne({authorId: Meteor.userId()})._id, {
                    $set: newData
                }, function (error, result) {
                    if (error)
                        toastr.error('Sorry, please try again.', 'Server Error!');
                });

                if (context.invalidKeys().length === 0)
                    Steps.check();
            } else {
                Steps.disableNextSteps(FlowRouter.getParam('step'));
            }
        }
    }
});
