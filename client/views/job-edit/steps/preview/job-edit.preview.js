Template.jobEditPreview.onCreated(function() {
    var template = this,
    currentUser = Meteor.user();

    template.currentUser = new ReactiveVar(currentUser);

    Seafarers.Controllers.JobEdit.subscribe(template, currentUser._id);
});


Template.jobEditPreview.onCreated(function() {
    var template = this,
    isDataInitiated = false,
    currentUser = template.currentUser.get(),
    userId = currentUser._id,
    db = Seafarers.Collections,
    Steps = Seafarers.Controllers.JobEdit.Steps,
    companyData, jobData;

    template.autorun(function() {
        companyData = db.Companies.findOne({userId: userId});
        jobData = db.Jobs.findOne({authorId: userId});


        // create template.currentUser object
        if (companyData && jobData && !isDataInitiated) {
            Steps.saveLastStep(FlowRouter.getParam('step'), companyData);

            template.currentUser.set(_.extend(
                companyData,
                jobData
            ));

            Steps.check();

            isDataInitiated = true;
        }
    }); // end of autorun
});


Template.jobEditPreview.helpers({
    currentUserData: function () {
        return  Template.instance().currentUser.get();
    },
    salaryRange: function() {
        if (this.salary && this.salary.max && this.salary.min)
            return '$ ' + this.salary.min + ' - ' + '$ ' + this.salary.max;
        else
            return '(To be evaluated based on experience)';
    }
});
