Template.jobEditCompanyInfo.onCreated(function() {
    var template = this,
    schema = new SimpleSchema([Seafarers.Schemas.CreateUser, Seafarers.Schemas.Companies]),
    currentUser = Meteor.user(),
    controller = Seafarers.Controllers.JobEdit,
    data, userInfo, form;

    template.Steps = controller.Steps;
    template.currentUser = new ReactiveVar(currentUser);
    template.schema = schema;
    template.context = schema.newContext();
    template.getFormData = getFormData;
    template.isInitialDataSet = new ReactiveVar(false);

    Seafarers.Collections.Companies.attachSchema(Seafarers.Schemas.DBCompanies, {replace: true});
    Seafarers.Collections.Jobs.attachSchema(Seafarers.Schemas.JobOwner, {replace: true});

    if (currentUser)
        controller.subscribe(template, currentUser._id);

    function getFormData(schema) {
    	form = template.$('#companyInfo');
        userInfo = {};
    	data = {
    		companyName: form.find('[name=companyName]').val(),
    		address: form.find('[name=address]').val(),
    		country: form.find('[name=country]').val(),
    		state: form.find('[name=state]').val(),
    		city: form.find('[name=city]').val(),
    		zipCode: form.find('[name=zipCode]').val()
    	};

        if (currentUser)
            userInfo = {
                email: currentUser.emails[0].address,
            	firstName: currentUser.profile.firstName,
            	lastName: currentUser.profile.lastName
            };
        else
            userInfo = {
                email: form.find('[name=email]').val(),
            	firstName: form.find('[name=firstName]').val(),
            	lastName: form.find('[name=lastName]').val()
            };

        data = _.extend(data, userInfo);

        schema.clean(data);

    	return data;
    }
});

Template.jobEditCompanyInfo.onRendered(function() {
    var template = this,
    currentUser = template.currentUser.get(),
    Steps = template.Steps,
    userId = currentUser ? currentUser._id : false,
    jobId = FlowRouter.current().params.jobId,
    currentStep = FlowRouter.getParam('step'),
    nextStep = Steps.getNext(currentStep).name,
    jobId = FlowRouter.getParam('jobId'),
    context = template.context,
    isInitialDataSet = template.isInitialDataSet,
    data, companyData, currentJob;


     $('[name=zipCode]').numberInput().maxInputLength(9);

    template.autorun(function(computation) {
        companyData = Seafarers.Collections.Companies.findOne({userId: userId});
        currentJob = Seafarers.Collections.Jobs.findOne({authorId: userId});

        if (currentUser) {
            // if unverified account is trying to create new job (again),
            // redirect him to the draft previously created
            if (companyData && !currentUser.emails[0].verified && jobId === 'new' && currentJob) {
                jobId = currentJob._id;
                FlowRouter.go('jobEditStep', {
                    jobId: currentJob._id,
                    step: FlowRouter.current().params.step
                });
            }

            // create currentUser object, (user data + company data)
            if (companyData && currentJob && !isInitialDataSet.get()) {
                template.currentUser.set(_.extend(
                    {companyId: companyData._id}, // save company's _id as companyId
                    companyData,
                    currentUser // user's _id will overwrite company's _id
                ));

                Steps.saveLastStep(currentStep, companyData);
                Steps.check();

                isInitialDataSet.set(true);
            }

        } else if (jobId !== 'new')
            FlowRouter.go('notFound');
        else
            isInitialDataSet.set(true);

    }); // end of autorun

    $('#next-step').click(function (e) {
        e.preventDefault();

        data = template.getFormData(template.schema);

        if (context.validate(data)) {
            if(currentUser) // if valid and has account, update applicant
                FlowRouter.go('jobEditStep', {
                    jobId: jobId,
                    step: nextStep
                });
            else // else if no acc, create one and add draft then...
                Meteor.call('createCompanyWithJob', data, function (error, result) {
                    if (result && !error)
                        // login new account then...
                        Meteor.loginWithPassword(data.email, result.tempPass, function (_error) {
                            if (!_error)
                                template.subscribe( // subscribe to company and...
                                    'specificCompany',
                                    {userId: Meteor.userId()},
                                    function (__error) {
                                        if (!__error) // subscribe to job also then...
                                            template.subscribe('specificJob', result.jobId,
                                            function(___error) {
                                                    if (!___error) {
                                                        // redirect to next step
                                                        Steps.check();
                                                        FlowRouter.go('jobEditStep', {
                                                            jobId: result.jobId,
                                                            step: nextStep
                                                        });
                                                    }
                                                }
                                            );
                                    }
                                );
                            else {
                                Meteor.users.remove(result.userId);
                                toastr.error('Sorry, please try again.', 'Server Error!');
                            }
                        });
                    else if (error && error.message === 'Email already exists. [403]') {
                        context.addInvalidKeys([{name: 'email', type: 'alreadyExists'}]);
                        toastr.error('Please fill form with valid data', 'Invalid!');
                    } else {
                        toastr.error(
                            'Sorry, saving updates failed, please try again.',
                            'Server Error!');
                    }
                });
        } else
            toastr.error('Please fill form with valid data', 'Invalid!');
    });
});

Template.jobEditCompanyInfo.onDestroyed(function () {
    $('#next-step').unbind('click');
});

Template.jobEditCompanyInfo.helpers({
    currentUserData: function () {
        return Template.instance().currentUser.get();
    },
    errClass: function (fieldName) {
        return Template.instance().context.keyIsInvalid(fieldName) ? 'error' : null;
    },
    errMsg: function (fieldName) {
        return Template.instance().context.keyErrorMessage(fieldName);
    }
});

Template.jobEditCompanyInfo.events({
    'change input, change select': function (event, template) {
        if (template.isInitialDataSet.get()) {
            var context = template.context,
    		field = $(event.target).prop('name'),
    		data = template.getFormData(template.schema),
            currentUser = template.currentUser.get(),
            Steps = template.Steps,
            newData;

            if (field === 'email')
                Meteor.call('checkEmail', data[field], function (error, result) {
                    if (!error && result)
                        context.addInvalidKeys([{name: 'email', type: 'alreadyExists'}]);
                });

            if (context.validateOne(data, field))
                if (currentUser) {
                    newData = {
                        // updatedAt: new Date()
                    };

                    newData[field] = data[field] || '';

                    Seafarers.Collections.Companies.update(currentUser.companyId, {
                        $set: newData
                    }, function (error, result) {
                        if (error)
                            toastr.error('Sorry, please try again.', 'Server Error!');
                    });

                    // these fields are also in job too, update job also
                    if (['companyName', 'state', 'country'].indexOf(field) !== -1)
                        Seafarers.Collections.Jobs.update(FlowRouter.getParam('jobId'), {
                            $set: newData
                        }, function (error, result) {
                            if (error)
                                toastr.error('Sorry, please try again.', 'Server Error!');
                        });

                    if (context.invalidKeys().length === 0)
                        Steps.check();
                } else
                    Steps.disableNextSteps(FlowRouter.getParam('step'));
        }
    }
});
