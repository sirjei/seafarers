Template.jobEditProgress.onCreated(function() {
    this.Steps = Seafarers.Controllers.JobEdit.Steps;
});

Template.jobEditProgress.helpers({
    steps: function () {
        var Steps = Template.instance().Steps;
        return Steps.getAll();
    },
    status: function () {
        var Steps = Template.instance().Steps,
        currentStep = FlowRouter.getParam('step');

        if (this.name === currentStep)
            return 'active';
        else if (!Steps.getOne(this.name).isEnabled)
            return 'disabled';
        else if (this.isCompleted)
            return 'completed';
        else
            return 'preview';
    },
    link: function () {
        if (this.isEnabled)
            return  FlowRouter.path('jobEditStep', {
                jobId: FlowRouter.getParam('jobId'),
                step: this.name
            });
        else
            return '#';
    }
});
