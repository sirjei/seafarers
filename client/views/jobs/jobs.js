Template.jobs.onCreated(function () {
    var template = this,
    currentUser = Meteor.user();

    template.Steps = Seafarers.Controllers.JobApplication.Steps;
    template.jobs = new ReactiveVar();

    if (currentUser)
        template.subscribe('specificApplicant', {userId: currentUser._id}, function () {
            template.Steps.check();
        });

    template.autorun(function () {
        template.jobs.set(Seafarers.Collections.Jobs.find({isPublished: true}).fetch());
    });
});

Template.jobs.helpers({
    showedJobs: function() {
        return Template.instance().jobs.get().length;
    },
    allJobs: function() { // temporary, not functional
        return Template.instance().jobs.get().length;
    },
    jobs: function() {
        return Template.instance().jobs.get();
    },
    farthestStep: function() {
        return Template.instance().Steps.getFarthestStep();
    }
});
