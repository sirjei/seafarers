Template.passwordForgot.onCreated(function () {
    this.__schema = Seafarers.Schemas.Users;
    this.__context = this.__schema.namedContext("forgotPassword");

    this.validate = function (field) {
        this.__schema.clean(field);
        this.__context.validateOne(field, "email");
        return this.__context.isValid();
    };

    this.hasError = function () {
        return this.__context.keyIsInvalid("email");
    };

    this.errorMessage = function () {
        return this.__context.keyErrorMessage("email");
    }
});

Template.passwordForgot.events({
    'click .submit': function (e, tmpl) {
        e.preventDefault();

        var field = {
            email: tmpl.$('[type=email]').val()
        };

        if(!tmpl.validate(field))
            return;

        Accounts.forgotPassword(field, function (err) {
            console.log("ERROR:",err);
        });
    }
});

Template.passwordForgot.helpers({
    hasError: function () {
        return Template.instance().hasError()? "error": null;
    },
    errorMessage: function () {
        return Template.instance().errorMessage();
    }
})
