Template.passwordReset.onCreated(function () {
    this.ResetForm = Seafarers.Validations.Passwords.Reset;
    this._tokenExpired = new ReactiveVar();
});

Template.passwordReset.onRendered(function () {
    this._token = FlowRouter.getParam('token');
});

Template.passwordReset.events({
    'click .set': function (e, tmpl) {
        e.preventDefault();

        var fields = {
            password: tmpl.$('[name=password]').val(),
            confirmPassword: tmpl.$('[name=confirmPassword]').val()
        };

        var isValid = tmpl.ResetForm.validate(fields);
        if (!isValid)
            return;

        Accounts.resetPassword(tmpl._token, fields.password, function (err) {
            if (err && err.error === 403) {
              //token expired
              return tmpl._tokenExpired.set(true);
            }

            if (err && err.error === 400) {
              //password must not be empty
              return tmpl.ResetForm.addInvalidKeys([{name: 'password', type: 'required'}]);
            }

            //show success template
            FlowLayout.render('layoutBlank',{mainContent: 'passwordResetSuccess'});
        });

    }
});

Template.passwordReset.helpers({
    tokenExpired: function () {
        return Template.instance()._tokenExpired.get();
    },
    hasError: function (field) {
        return Template.instance().ResetForm.hasError(field) ? "error" : "";
    },
    getErrorMessage: function (field) {
        return Template.instance().ResetForm.getErrorMessage(field);
    },
});
