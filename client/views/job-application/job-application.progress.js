Template.jobApplicationProgress.onCreated(function() {
    this.Steps = Seafarers.Controllers.JobApplication.Steps;
});

Template.jobApplicationProgress.helpers({
    isLastStep: function () {
        return Template.instance().Steps.lastStep ===
            FlowRouter.getParam('step');

    },
    steps: function () {
        return Template.instance().Steps.getAll();
    },
    status: function () {
        var currentStep = FlowRouter.getParam('step');

        if (this.name === currentStep)
            return 'active';
        else if (!Template.instance().Steps.getOne(this.name).isEnabled)
            return 'disabled';
        else if (this.isCompleted)
            return 'completed';
        else
            return 'preview';
    },
    link: function () {
        if (this.isEnabled)
            return FlowRouter.path('jobApplicationStep', {
                jobId: FlowRouter.getParam('jobId'),
                step: this.name
            });
        else
            return '#';
    },
    isJobApplied: function () {
        var currentJob = jobData = Seafarers.Collections.Jobs
            .findOne(FlowRouter.getParam('jobId')),
        jobApplications = currentJob && currentJob.applications,
        isJobApplied, application;

        if (jobApplications) {
            application = _.find(jobApplications, function (e) {
                // find for jobApplication of currentUser
                return e.userId === Meteor.userId();
            });

            return application && application.isSent;
        }
    }
});

Template.jobApplicationProgress.events({
    'click #send-application': function () {
        Meteor.call('sendApplication', FlowRouter.getParam('jobId'),
            function (err, res) {
                if (!err && res) {
                    $('#application-modal').modal({
                        onHidden: function () {
                            FlowRouter.go('appliedJobs');
                        }
                    }).modal('show');
                } else {
                    toastr.error('Sorry, server error occured, please try again later.',
                        'Server Error!');
                }
            });
    }
});