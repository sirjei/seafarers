Template.jobApplicationBasicInfo.onCreated(function() {
    var template = this,
    schema = new SimpleSchema([Seafarers.Schemas.CreateUser,
        Seafarers.Schemas.BasicInfo]),
    userData = Meteor.user(),
    controller = Seafarers.Controllers.JobApplication,
    form, formData, userInfo;

    template.Steps = controller.Steps;
    template.schema = schema;
    template.context = schema.newContext();
    template.getFormData = getFormData;
    template.isInitalDataSet = new ReactiveVar(false);
    template.savedData = new ReactiveVar(userData);

    Seafarers.Collections.Applicants.attachSchema(Seafarers.Schemas.DBBasicInfo,
        {replace: true});
    controller.subscribeApplicant();

    function getFormData() {
        form = $('#basicInfo');

        if (userData)
            userInfo = {
                email: userData.emails[0].address,
            	firstName: userData.profile.firstName,
            	lastName: userData.profile.lastName
            };
        else
            userInfo = {
                email: form.find('[name=email]').val(),
            	firstName: form.find('[name=firstName]').val(),
            	lastName: form.find('[name=lastName]').val()
            };

        formData = {
    		gender: form.find('[name=gender]').val(),
    		birthday: form.find('[name=birthday]').val(),
    		contactNo: form.find('[name=contactNo]').val(),
    		address: form.find('[name=address]').val(),
    		country: form.find('[name=country]').val(),
    		state: form.find('[name=state]').val(),
    		city: form.find('[name=city]').val(),
    		zipCode: form.find('[name=zipCode]').val()
    	};

        return schema.clean(_.extend(formData, userInfo));
    }
});

Template.jobApplicationBasicInfo.onRendered(function() {
    var template = this,
    savedData = template.savedData,
    Steps = template.Steps,
    // initially, it only contains user data set from onCreated
    userData = savedData.get(),
    userId = userData && userData._id,
    isInitalDataSet = template.isInitalDataSet,
    birthdayPickadate = $('[name=birthday]').pickadate(),
    context = template.context,
    currentStep = FlowRouter.getParam('step'),
    nextStep = Steps.getNext(currentStep).name,
    jobId = FlowRouter.getParam('jobId'),
    formData, applicantData;

    $('[name=zipCode]').numberInput().maxInputLength(9);

    template.autorun(function () {
        applicantData = Seafarers.Collections.Applicants.findOne(
            {userId: userId});

        if (userData && applicantData && !isInitalDataSet.get()) {
            savedData.set(_.extend(
                {applicantId: applicantData._id},
                applicantData,
                userData // user's _id will overwrite applicant's _id
            ));

            Steps.check();

    	    birthdayPickadate.pickadate('picker').set('select',
                applicantData.birthday);
            $('[name=gender]').setSelected(applicantData.gender);

            isInitalDataSet.set(true);

            Steps.saveLastStep(currentStep, applicantData);
            Seafarers.Controllers.JobApplication
                .addApplicationDraft(FlowRouter);
        } else if (!userData)
            isInitalDataSet.set(true);
    });

    $('#next-step').click(function (e) {
        e.preventDefault();

        formData = template.getFormData();

        if (context.validate(formData))
            if(userData) // if valid and has account, update applicant
                FlowRouter.go('jobApplicationStep', {
                    jobId: jobId,
                    step: nextStep
                });
            else // else if no acc, create one and add draft then...
                Meteor.call('createApplicantWithDraft',
                    _.extend(formData, {lastStep: currentStep}), jobId,
                    function (error, result) {
                        if (result && !error)
                            // login new account then...
                            Meteor.loginWithPassword(formData.email, result.tempPass, function (_error) {
                                if (!_error)
                                    // subscribe to applicants *for steps checking then...
                                    Meteor.subscribe(
                                        'specificApplicant',
                                        {userId: Meteor.userId()},
                                        function (__error) {
                                            if (!error) { // check and redirect to next step
                                                Steps.check();
                                                FlowRouter.go('jobApplicationStep', {
                                                    jobId: jobId,
                                                    step: nextStep
                                                });
                                            }
                                        }
                                    );
                                else {
                                    Meteor.users.remove(result.userId);
                                    toastr.error('Sorry, please try again.', 'Server Error!');
                                }
                            });
                        else if (error && error.message === 'Email already exists. [403]') {
                            context.addInvalidKeys([{name: 'email', type: 'alreadyExists'}]);
                            toastr.error('Please fill form with valid data', 'Invalid!');
                        } else
                            toastr.error(
                                'Sorry, saving updates failed, please try again.',
                                'Server Error!');
                    });
        else
            toastr.error('Please fill form with valid data', 'Invalid!');

        console
    });
});

Template.jobApplicationBasicInfo.onDestroyed(function () {
    $('#next-step').unbind('click');
});

Template.jobApplicationBasicInfo.helpers({
    savedData: function () {
        return Template.instance().savedData.get();
    },
    errClass: function (fieldName) {
        return Template.instance().context.keyIsInvalid(fieldName) ?
            'error' : null;
    },
    errMsg: function (fieldName) {
        return Template.instance().context.keyErrorMessage(fieldName);
    }
});

Template.jobApplicationBasicInfo.events({
    'change input, change select': function (event, template) {
        if (template.isInitalDataSet.get()) {
            var context = template.context,
    		field = $(event.target).prop('name'),
    		formData = template.getFormData(),
            savedData = template.savedData.get(),
            Steps = template.Steps,
            newData;

            if (field === 'email')
                Meteor.call('checkEmail', formData[field], function (
                    error, result
                ) {
                    if (!error && result)
                        context.addInvalidKeys([{name: 'email',
                            type: 'alreadyExists'}]);
                });

            if (context.validateOne(formData, field))
                if (savedData) {
                    newData = {};

                    newData[field] = formData[field] || '';

                    Seafarers.Collections.Applicants.update(
                    savedData.applicantId, {
                        $set: newData
                    }, function (error, result) {
                        if (error)
                            toastr.error('Sorry, please try again.',
                                'Server Error!');
                    });

                    if (context.invalidKeys().length === 0)
                        Steps.check();
                } else
                    Steps.disableNextSteps(FlowRouter.getParam('step'));
        }
    }
});
