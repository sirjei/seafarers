Template.jobApplicationTrainingCerts.onCreated(function() {
    this.Steps = Seafarers.Controllers.JobApplication.Steps;
    this.isDataInitiated = new ReactiveVar();

    Seafarers.Collections.Applicants.attachSchema(Seafarers.Schemas.DBTrainingCerts , {replace: true});
    Seafarers.Controllers.JobApplication.subscribeApplicant();
});

Template.jobApplicationTrainingCerts.onRendered(function() {
    var template = this,
    userId = Meteor.userId(),
    Applicants = Seafarers.Collections.Applicants,
    isDataInitiated = template.isDataInitiated,
    currentStep = FlowRouter.getParam('step'),
    Steps = template.Steps,
    nextStep = Steps.getNext(currentStep).name,
    trainings = [],
    applicantData, applicantId;

    template.autorun(function() {
        Seafarers.Controllers.JobApplication.initCheck(FlowRouter);

        applicantData = Applicants.findOne({userId: userId});

        // initiate data
        if (applicantData && !isDataInitiated.get()) {
            applicantId = applicantData._id;

            if (applicantData.trainingCerts)
                trainings = applicantData.trainingCerts;
            else // insert empty array of trainingCerts if there's none
                Applicants.update({
                    _id: applicantId
                }, {
                    $set: {trainingCerts: []}
                }, function (error, affected) {
                    if (error)
                        throw error;
                });

            template.Trainings = new Seafarers.Core.Factories.StrArrInput({
                strings: trainings,
                collection: Applicants,
                documentId: applicantId,
                field: 'trainingCerts',
                name: 'Trainings'
            });

            template.isDataInitiated.set(true);
            Steps.saveLastStep(
                currentStep,
                applicantData);
        }
    }); // end of autorun

    $('#next-step').click(function () {
        FlowRouter.go('jobApplicationStep', {
            jobId: FlowRouter.getParam('jobId'),
            step: nextStep
        });
    });
});


Template.jobApplicationTrainingCerts.onDestroyed(function () {
    $('#next-step').unbind('click');
});


Template.jobApplicationTrainingCerts.helpers({
    trainings: function() {
        var template = Template.instance();

        if (template.isDataInitiated.get())
            return template.Trainings.get();
    }
});

Template.jobApplicationTrainingCerts.events({
    'change #training-input': function (event, template) {
        if (template.isDataInitiated.get()) {
            $input = $(event.target);

            template.Trainings.add($input.val());
            $input.val('');
        }
    },
    'click a.remove': function (event, template) {
        event.preventDefault();

        if (template.isDataInitiated.get())
            template.Trainings.remove($.trim($(event.target).parents('.custom').text()));
    }
});
