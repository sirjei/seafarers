Template.jobApplicationPrimaryDocs.onCreated(function() {
    var template = this,
    schemas = Seafarers.Schemas,
    schema = schemas.PrimaryDocs,
    controller = Seafarers.Controllers.JobApplication,
    Steps = controller.Steps,
    form, userInfo, max, min, data;

    template.Steps = Steps;
    template.schema = schema;
    template.context = schema.newContext();
    template.isInitalDataSet = new ReactiveVar(false);
    template.savedData = new ReactiveVar(false);
    template.getFormData = function() {
    	form = $('#primaryDocs');
        userInfo = {};
        max = form.find('[name=salary-max]').val();
        min = form.find('[name=salary-min]').val();
        data = {
    		rank: form.find('[name=rank]').val(),
    		vesselType: form.find('[name=vesselType]').val(),
            primaryDocs: {}
    	};

        if (max || min)
            data.salary = {
                max: max,
                min: min
            };

        _.each(['passport', 'visa', 'seamansBook'], function (docName) {
            data.primaryDocs[docName] = {
                number: form.find('[name=primaryDocs-'+docName+'-number]').val(),
                dateIssued: form.find('[name=primaryDocs-'+docName+'-dateIssued]').val(),
                validUntil: form.find('[name=primaryDocs-'+docName+'-validUntil]').val()
            };
        });

        schema.clean(data);

    	return data;
    };

    Seafarers.Collections.Applicants.attachSchema(schemas.DBPrimaryDocs, {replace: true});
    controller.subscribeApplicant();
});

Template.jobApplicationPrimaryDocs.onRendered(function() {
    var template = this,
    userId = Meteor.userId(),
    currentStep = FlowRouter.getParam('step'),
    Steps = template.Steps,
    applicantData, dateIssuedPickadate, validUntilPickadate, data,
    savedData;

    $('[name=salary-max]').numberInput().maxInputLength(7);
    $('[name=salary-min]').numberInput().maxInputLength(7);

    template.autorun(function () {
        Seafarers.Controllers.JobApplication.initCheck(FlowRouter);

        applicantData = Seafarers.Collections.Applicants.findOne({userId: userId});

        // set template.savedData object
        if (applicantData && !template.isInitalDataSet.get()) {
            savedData = _.extend(
                {
                    _id: userId,
                    applicantId: applicantData._id
                },
                applicantData
            );

            // set documents' dates
            _.each(['passport', 'visa', 'seamansBook'], function (docName) {
                dateIssuedPickadate =
                    $('[name=primaryDocs-'+docName+'-dateIssued]').pickadate();
                validUntilPickadate =
                    $('[name=primaryDocs-'+docName+'-validUntil]').pickadate();

            	if (
                    savedData.primaryDocs &&
                    savedData.primaryDocs[docName] &&
                    savedData.primaryDocs[docName].dateIssued
                )
            	    dateIssuedPickadate.pickadate('picker').set('select',
                    savedData.primaryDocs[docName].dateIssued);
                if (
                    savedData.primaryDocs &&
                    savedData.primaryDocs[docName] &&
                    savedData.primaryDocs[docName].validUntil
                )
            	    validUntilPickadate.pickadate('picker').set('select',
                    savedData.primaryDocs[docName].validUntil);
            });

            Steps.saveLastStep(
                currentStep,
                applicantData);
            template.savedData.set(savedData);
            template.isInitalDataSet.set(true);
        }
    }); // end of autorun

    $('#next-step').click(function () {
        data = template.getFormData(template.schema);

        if (template.context.validate(data))
            FlowRouter.go('jobApplicationStep', {
                jobId: FlowRouter.getParam('jobId'),
                step: Steps.getNext(currentStep).name
            });
        else
            toastr.error('Please fill form with valid data', 'Invalid!');
    });
});


Template.jobApplicationPrimaryDocs.onDestroyed(function () {
    $('#next-step').unbind('click');
});


Template.jobApplicationPrimaryDocs.helpers({
    savedData: function () {
        return Template.instance().savedData.get();
    },
    primaryDocs: function() {
        return _.map([
            {
                name: 'visa',
                displayName: 'VISA'
            },
            {
                name: 'passport',
                displayName: 'PASSPORT'
            },
            {
                name: 'seamansBook',
                displayName: 'SEAMAN\'S BOOK'
            }
        ], function (doc) {
            doc.number = 'primaryDocs.' + doc.name +'.number';
            doc.dateIssued = 'primaryDocs.' + doc.name +'.dateIssued';
            doc.validUntil = 'primaryDocs.' + doc.name +'.validUntil';

            var primaryDocs = Template.instance().savedData.get().primaryDocs;

            if (
                primaryDocs &&
                primaryDocs[doc.name] &&
                primaryDocs[doc.name].number
            )
                doc.numberValue = primaryDocs[doc.name].number;
            return doc;
        });
    },
    errClass: function (fieldName) {
        var context = Template.instance().context;
        return context.keyIsInvalid(fieldName) ? 'error' : null;
    },
    errMsg: function (fieldName) {
        var context = Template.instance().context;
        return context.keyErrorMessage(fieldName);
    }
});

Template.jobApplicationPrimaryDocs.events({
    'change input': function (event, template) {
        if (template.isInitalDataSet.get()) {
            var context = template.context,
    		name = $(event.target).prop('name'),
    		field = name,
    		data = template.getFormData(),
            savedData = template.savedData.get(),
            Steps = template.Steps,
            newData,
            value,
            updateApplicant = function(propPath) {
                if (context.validateOne(data, propPath)) {
                    newData = {};
                    // to be able assign and access obj prop with string
                    // like "obj.deep.prop"
                    value = Seafarers.Helpers.Common.getProp(data, propPath) ||
                        '';

                    newData[propPath] = value;

                    Seafarers.Collections.Applicants.update({
                        _id: savedData.applicantId
                    }, {
                        $set: newData
                    }, function (error, affected) {

                        if (error)
                            toastr.error('Sorry, saving updates failed, ' +
                                'please try again.', 'Server Error!');
                    });

                    if (context.invalidKeys().length === 0)
                        Steps.check();
                } else
                    Steps.disableNextSteps(FlowRouter.getParam('step'));
            };

            if (name.indexOf('-') !== -1)
                field = field.replace(/-/g, '.');

            updateApplicant(field);

            Seafarers.Helpers.Forms.partnerFields([
                {one: 'min' , two: 'max'},
                {one: 'dateIssued' , two: 'validUntil'},
            ], name, updateApplicant);
        }
    }
});
