Template.jobApplicationReviewCV.onCreated(function() {
    var controller = Seafarers.Controllers.JobApplication;

    this.Steps = controller.Steps;
    this.currentUser = new ReactiveVar();

    controller.subscribeApplicant();
});


Template.jobApplicationReviewCV.onRendered(function() {
    var template = this,
    Steps = this.Steps,
    isDataInitiated = false,
    currentUser = Meteor.user(),
    currentStep = FlowRouter.getParam('step'),
    applicantData;

    template.autorun(function() {
        Seafarers.Controllers.JobApplication.initCheck(FlowRouter);

        applicantData = Seafarers.Collections.Applicants.findOne({userId: currentUser._id});

        // create template.currentUser object
        if (applicantData && !isDataInitiated) {
            template.currentUser.set(_.extend(
                currentUser,
                applicantData
            ));

            Steps.saveLastStep(currentStep, applicantData);

            isDataInitiated = true;
        }
    }); // end of autorun
});

Template.jobApplicationReviewCV.helpers({
    currentUserData: function () {
        return  Template.instance().currentUser.get();
    },
    salaryRange: function() {
        if (this.salary && this.salary.max && this.salary.min)
            return '$ ' + this.salary.min + ' - ' + '$ ' + this.salary.max;
        else
            return '(unspecified)';
    },
    jobHistorySignDate: function () {
        var formatDate = Seafarers.Helpers.Common.formatDate,
        result;

        if (this.signIn || this.signOff) {
            if (this.signIn)
                result = formatDate(this.signIn, 'MM/DD/YY');
            else
                result = '(unspecified)';

            result += ' - ';

            if (this.signOff)
                result += formatDate(this.signOff, 'MM/DD/YY');
            else
                result += '(unspecified)';

            return result;
        }
    },
    jobHistorySalaryRange: function () {
        if (this.salary) {
            if (this.salary.min)
                result = this.salary.min;
            else
                result = '(unspecified)';

            result += ' - ';

            if (this.salary.max)
                result += this.salary.max;
            else
                result += '(unspecified)';

            return result;
        } else
            return '(unspecified)';
    },
    finalJobHistories: function () {
        return _.filter(this.jobHistories, function (jobHistory) {
            return jobHistory.isDraft === false;
        });
    }
});
