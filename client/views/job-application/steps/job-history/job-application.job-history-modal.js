Template.JobHistoryModal.onCreated(function () {
    var template = this,
    schema = Seafarers.Schemas.JobHistory,
    form,
    currentUser,
    userInfo,
    max,
    min,
    data;

    template.schema = schema;
    template.context = schema.newContext();
    template.Steps = Seafarers.Controllers.JobApplication.Steps;
    template.isInitialDataSet = new ReactiveVar(false);
    template.getFormData = function() {
        form = $('.content');
        currentUser = Meteor.user();
        userInfo = {};
        max = form.find('[name=salary-max]').val();
        min = form.find('[name=salary-min]').val();
        data = {
        	rank: form.find('[name=rank]').val(),
        	agency: form.find('[name=agency]').val(),
        	signIn: form.find('[name=signIn]').val(),
        	signOff: form.find('[name=signOff]').val(),
        	vesselName: form.find('[name=vesselName]').val(),
        	vesselType: form.find('[name=vesselType]').val()
        };

        if (max || min)
            data.salary = {
                max: max,
                min: min
            };

        schema.clean(data);

        return data;
    };
});

Template.JobHistoryModal.onRendered(function () {
    var data = this.data,
    signInPickadate,
    signOffPickadate;

    $('[name=salary-max]').numberInput().maxInputLength(7);
    $('[name=salary-min]').numberInput().maxInputLength(7);

    signInPickadate = $('[name=signIn]').pickadate();
    signOffPickadate = $('[name=signOff]').pickadate();

	if (data.signIn)
	    signInPickadate.pickadate('picker').set('select', data.signIn);
	if (data.signOff)
	    signOffPickadate.pickadate('picker').set('select', data.signOff);

    this.isInitialDataSet.set(true);
});

Template.JobHistoryModal.onDestroyed(function () {
    this.Steps.check();
});

Template.JobHistoryModal.helpers({
    errClass: function (fieldName) {
        var context = Template.instance().context;
        return context.keyIsInvalid(fieldName) ? 'error' : null;
    },
    errMsg: function (fieldName) {
        var context = Template.instance().context;
        return context.keyErrorMessage(fieldName);
    }
});

Template.JobHistoryModal.events({
    'change input': function (event, template) {
        if (template.isInitialDataSet.get()) {
            var schema = template.schema,
            context = template.context,
    		name = $(event.target).prop('name'),
    		field = name,
    		formData = template.getFormData(schema),
            currentUser = template.currentUser,
            Steps = template.Steps,
            actionData = template.data,
            applicantId = actionData.applicantId,
            jobHistoryId = actionData._id,
            newId, value, newData;

            if (name.indexOf('-') !== -1)
                field = field.replace(/-/g, '.');

            updateApplicant(field);

            Seafarers.Helpers.Forms.partnerFields([
                {one: 'signIn' , two: 'signOff'},
                {one: 'min' , two: 'max'},
            ], name, updateApplicant);
        }

        function updateApplicant(propPath) {

            formData.isDraft = actionData.isDraft;

            if (formData.isDraft && _.isEmpty(_.omit(formData, 'isDraft'))) {
                Meteor.call('deleteJobHistory', applicantId, jobHistoryId, function (error) {
                    if (error)
                        throw error;
                });
                actionData._id = false;
                return;
            }

            if (context.validateOne(formData, propPath)) {
                if (jobHistoryId) { // modify current job history
                    value = Seafarers.Helpers.Common.getProp(formData, propPath) || ''; // to be able assign and access obj prop with string like "obj.deep.prop"
                    newData = {};

                    newData['jobHistories.$.' + propPath] = value;

                    Meteor.call('updateJobHistory', applicantId, jobHistoryId, newData, function (error) {
                        if (error)
                            throw error;
                    });
                } else { // add job history draft
                    newId = Random.id();

                    Seafarers.Collections.Applicants.update(
                        {_id: applicantId},
                        {$push: {
                            jobHistories: _.extend(formData, {
                                _id: newId,
                                isDraft: true
                            }
                        )}},
                        function (error, affected) {
                            if (!error && affected) {
                                actionData._id = jobHistoryId = newId;
                            } else
                                throw error;
                        }
                    );

                }

                template.Steps.check();
            } else
                template.Steps.disableNextSteps(FlowRouter.getParam('step'));
        }
    },
    'click #delete': function (event, template) {
        var data = template.data;

        if (data._id)
            Meteor.call('deleteJobHistory', data.applicantId, data._id, function (error) {
                if (error)
                    throw error;
            });

        $('#jobHistoryForm').modal('hide');
    },
    'click #save': function (event, template) {
        var formData = template.getFormData(template.schema),
        newData,
        actionData,
        jobHistoryId;

        if (template.context.validate(formData)) {
            newData = {'jobHistories.$.isDraft': false};
            actionData = template.data;
            jobHistoryId = actionData._id;

            if (!actionData.isDraft) {
                $('#jobHistoryForm').modal('hide');
                return;
            } else
                formData.isDraft = false;

            _.each(formData, function(value, key, obj) {
                newData['jobHistories.$.' + key] = value;
            });

            Meteor.call('updateJobHistory', actionData.applicantId, jobHistoryId, newData, function (error, result) {
                if (error)
                    throw error;
            });

            $('#jobHistoryForm').modal('hide');
        } else
            toastr.error('Please fill form with valid data', 'Invalid!');
    }
});
