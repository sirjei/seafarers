Template.jobApplicationJobHistory.onCreated(function() {
    var template = this,
    controller = Seafarers.Controllers.JobApplication;

    template.Steps = controller.Steps;
    template.actionData = new ReactiveVar(false);
    template.isDataInitiated = new ReactiveVar(false);
    template.currentUser = Meteor.user();

    Seafarers.Collections.Applicants.attachSchema(Seafarers.Schemas.DBJobHistories, {replace: true});
    controller.subscribeApplicant();
});

Template.jobApplicationJobHistory.onRendered(function() {
    var template = this,
    isDataInitiated = template.isDataInitiated,
    userId = template.currentUser._id,
    jobId = FlowRouter.getParam('jobId'),
    Applicants = Seafarers.Collections.Applicants,
    Steps = template.Steps,
    currentStep = FlowRouter.getParam('step'),
    nextStep = Steps.getNext(currentStep),
    applicantData, applicantId;


    template.autorun(function() {
        Seafarers.Controllers.JobApplication.initCheck(FlowRouter);

        applicantData = Applicants.findOne({userId: userId});

        // create template.currentUser object
        if (applicantData && !isDataInitiated.get()) {
            applicantId = applicantData._id;

            if (!applicantData.jobHistories) // insert empty array of jobHistories if there's none
                Applicants.update({
                    _id: applicantId
                }, {
                    $set: {jobHistories: []}
                }, function (error) {
                    if (error)
                        throw error;
                });

            template.currentUser = {
                _id: userId,
                applicantId: applicantId
            };

            Steps.saveLastStep(currentStep, applicantData);

            isDataInitiated.set(true);
        }
    }); // end of autorun

    this.$('#jobHistoryForm').modal({
        onHidden: function () {
            template.actionData.set(false);
        },
        detachable: false
    });

    $('#next-step').click(function () {
        FlowRouter.go('jobApplicationStep', {
            jobId: FlowRouter.getParam('jobId'),
            step: nextStep.name
        });
    });
});

Template.jobApplicationJobHistory.onDestroyed(function () {
    $('#next-step').unbind('click');
});

Template.jobApplicationJobHistory.helpers({
    actionData: function () {
        return Template.instance().actionData.get();
    },
    applicant: function () {
        return Seafarers.Collections.Applicants.findOne({userId: Template.instance().currentUser._id});
    },
    signDate: function () {
        var formatDate = Seafarers.Helpers.Common.formatDate,
        result;

        if (this.signIn || this.signOff) {
            if (this.signIn)
                result = formatDate(this.signIn, 'MM/DD/YY');
            else
                result = '(unspecified)';

            result += ' - ';

            if (this.signOff)
                result += formatDate(this.signOff, 'MM/DD/YY');
            else
                result += '(unspecified)';

            return result;
        }
    },
    salaryRange: function () {
        if (this.salary) {
            if (this.salary.min)
                result = this.salary.min;
            else
                result = '(unspecified)';

            result += ' - ';

            if (this.salary.max)
                result += this.salary.max;
            else
                result += '(unspecified)';

            return result;
        }
    }
});


Template.jobApplicationJobHistory.events({
    'click #addJobHistory': function (event, template) {
        if (template.isDataInitiated.get()) {
            template.actionData.set({
                isDraft: true,
                isNew: true,
                _id: false,
                applicantId: template.currentUser.applicantId
            });

            $('#jobHistoryForm').modal('show');
        }
    },
    'click .job-history-card': function (event, template) {
        template.actionData.set(_.extend(this, {
            isNew: false,
            applicantId: template.currentUser.applicantId
        }));

        $('#jobHistoryForm').modal('show');
    }
});
