Template.jobApplicationPromote.onCreated(function() {
    var controller = Seafarers.Controllers.JobApplication;

    this.Steps = controller.Steps;

    controller.subscribeApplicant();
});

Template.jobApplicationPromote.onRendered(function () {
    var Steps = this.Steps,
    currentStep = FlowRouter.getParam('step');

    this.autorun(function() {
        Seafarers.Controllers.JobApplication.initCheck(FlowRouter);

        applicantData = Seafarers.Collections.Applicants.findOne({userId: Meteor.userId()});

        if (applicantData)
            Steps.saveLastStep(currentStep, applicantData);
    }); // end of autorun

    $('#next-step').click(function () {
        FlowRouter.go('jobApplicationStep', {
            jobId: FlowRouter.getParam('jobId'),
            step: Steps.getNext(currentStep).name
        });
    });
});


Template.jobApplicationPromote.onDestroyed(function() {
    $('#next-step').unbind('click');
});
