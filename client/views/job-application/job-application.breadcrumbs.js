Template.jobApplicationBreadcrumbs.helpers({
    stepDisplayName: function () {
        var currentStep = FlowRouter.getParam('step');
        return currentStep && Seafarers.Controllers.JobApplication.Steps
        	.getOne(currentStep).displayName;
    }
});
