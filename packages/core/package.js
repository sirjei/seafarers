Package.describe({
    name: 'sj-app:core',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: 'Core Library for seafarers jobsite'
});

Package.onUse(function(api) {
    api.versionsFrom('1.0.5');

    api.use('aldeed:simple-schema');
    api.use('momentjs:moment');
    api.use('aldeed:collection2');
    api.use('cfs:standard-packages');
    api.use('tracker');
    api.use('underscore');
    api.use('blaze');
    api.use('chrismbeckett:toastr');
    api.use("accounts-password");
    api.use("meteorhacks:ssr");
    api.use("templating");
    api.use("reywood:publish-composite");

    api.addFiles([
        'namespace.js'
    ]);

    api.addFiles([
        'config/config.email.js',
    ], 'server');

    api.addFiles([
        //assets
        'assets/email/email.forgot-password.html',
    ], ['client','server'], {isAsset: true});

    api.addFiles([
        //helpers
        'helpers/helpers.route.js',
        'helpers/helpers.common.js',

        //schemas
        'schemas/schema.messages-config.js',
        'schemas/schema.helpers.js',
        'schemas/shared/schema.timestamps.js',
        'schemas/shared/schema.location.js',
        'schemas/shared/schema.salary.js',
        'schemas/shared/schema.last-step.js',
        'schemas/schema.companies.js',
        'schemas/schema.jobs.js',
        'schemas/schema.users.js',
        'schemas/applicants/basic-info.js',
        'schemas/applicants/primary-docs.js',
        'schemas/applicants/training-certs.js',
        'schemas/applicants/job-history.js',

        //collections
        'collections/collections.users.js',
        'collections/db.company-gallery.js',
        'collections/collections.companies.js',
        'collections/collections.jobs.js',
        'collections/collections.applicants.js',

        //factories
        'factories/factory.validator.js', // is it used?
        'factories/factory.steps.js',
        'factories/factory.string-array-input.js',

        //resources
        'resources/countries.js',
        'resources/currencies.js',
        'resources/vessels.js',
        'resources/job-titles.js',
        'resources/trainings.js',
        'resources/states.js'
    ]);

    api.addFiles([
        //publications
        'publications/pub.jobs.js',
        'publications/pub.applicants.js',
        'publications/pub.companies.js',

        //seeders
        'seeders/seeder.users.js',
    ], 'server');


    api.addFiles([
        //publications
        'helpers/helpers.forms.js',
        'helpers/helpers.templates.js'
    ], 'client');

    api.export('Seafarers');
});
