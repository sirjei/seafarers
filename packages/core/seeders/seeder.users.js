var db = Seafarers.Collections;

Meteor.startup(function () {
    if (Meteor.users.find().count() === 0) {

        var userData = getUserData(),
            jobPosts = getJobPosts(),
            hasError;

        try {

            for (var i = 0; i < userData.length; i++) {
                var userId = Accounts.createUser(userData[i].user);
                userData[i].user._id = userId;

                if (userData[i].company) {
                    userData[i].company.userId = [userId];
                    var companyId = db.Companies.insert(userData[i].company);
                    userData[i].company._id = companyId;

                    if (userData[i].user.email == "jerome@company.com") {
                        createJobPosts(userData[i], _.filter(jobPosts, function(data, index){
                            if (index < 3)
                                return data;
                        }));
                    } else {
                        createJobPosts(userData[i], _.filter(jobPosts, function(data, index){
                            if (index >= 3)
                                return data;
                        }));
                    }


                } else {
                    userData[i].applicant.userId = userId;
                    db.Applicants.insert(userData[i].applicant);
                }
            }//end of user creation loop

        } catch (e) {
            hasError = true;
            throw e;
        } finally {
            if (hasError) {
                Meteor.users.remove({});
                db.Applicants.remove({});
                db.Companies.remove({});
                db.Jobs.remove({});
            }
        }

    }
});

function createJobPosts(data, jobs) {
    for (var i = 0; i < jobs.length; i++) {
        jobs[i].authorId = data.user._id;
        jobs[i].companyId = data.company._id;
        jobs[i].companyName = data.company.companyName;
        db.Jobs.insert(jobs[i]);
    }
}

function getUserData() {
    return [{
        user: {
            email: "jerome@company.com",
            password: "password",
            profile: {
                firstName: "Jerome",
                lastName: "Gamo",
                type: "company",
                role: "administrator"
            }
        },
        company: {
            companyName: "Chronos Maritime",
            address: "Gr. Flr, Unit 1-E, Manere Bldg I, 18 Matahimik St., Cor V. Luna Road",
            country: "ph",
            state: "Quezon City",
            city: "Diliman",
            zipCode: "1605",
            createdAt: new Date(),
            updatedAt: new Date()
        }
    },{
        user: {
            email: "chris@company.com",
            password: "password",
            profile: {
                firstName: "Chris",
                lastName: "Dela Paz",
                type: "company",
                role: "administrator"
            }
        },
        company: {
            companyName: "Bright Maritime Corp.",
            address: "3rd and 4th Floor Emerald Building, #24 Emerald Avenue, Ortigas Center",
            country: "jp",
            state: "Manila",
            city: "Pasig City",
            zipCode: "1605",
            createdAt: new Date(),
            updatedAt: new Date()
        }
    },{
        user: {
            email: "raven@applicant.com",
            password: "password",
            profile: {
                firstName: "John Raven",
                lastName: "Gamo",
                type: "applicant"
            }
        },
        applicant: {
            gender: "male",
            birthday: new Date(1995,10,11),
            rank: "Master",
            vesselType: "Bulk",
            salary: {
                min: 1000,
                max: 2000
            },
            contactNo: "09266372242",
            address: "Pagayon Compound, Manggahan",
            country: "ph",
            state: "Rizal",
            city: "Rodriguez",
            zipCode: "1860",
            createdAt: new Date(),
            updatedAt: new Date()
        }
    },{
        user: {
            email: "dwayne@applicant.com",
            password: "password",
            profile: {
                firstName: "Dwayne Marie",
                lastName: "Cruz",
                type: "applicant"
            }
        },
        applicant: {
            gender: "female",
            birthday: new Date(1995,10,14),
            rank: "Chief Officer",
            vesselType: "VLCC",
            salary: {
                min: 1000,
                max: 2000
            },
            contactNo: "09298222823",
            address: "Blk 6 Lot 28, Daang Bakal St., Manggahan",
            country: "us",
            state: "Rizal",
            city: "Rodriguez",
            zipCode: "1860",
            createdAt: new Date(),
            updatedAt: new Date()
        }
    }];
}

function getJobPosts() {
    return [{
        companyId: "",
        companyName: "",
        title: "Master",
        vesselType: "Bulk",
        hasSalary: true,
        salary: {
            min: 1000,
            max: 2000
        },
        description: "Day to day duties will include the supervision of a small Marine team and you will make a key contribution to the provision of a quality Marine and Port service to our customers. You will also receiving training to become  a competent Pilot within the ABP Lowestoft CHA. The job will require a flexible and committed approach, and performing a variety of other marine related tasks, including the deputising for the Harbour Master in his absence.",
        employmentBenefits: [],
        requiredTrainings: ["Master’s Certificate of Competency (STCW 11/2 over 3000 GT)"],
        otherQualifications: [],
        createdAt: new Date(),
        updatedAt: new Date(),
        isPublished: true,
        country: 'us',
        state: 'RIZAL'
    },{
        companyId: "",
        companyName: "",
        title: "Bosun",
        vesselType: "Bulk",
        hasSalary: true,
        salary: {
            min: 1000,
            max: 2000
        },
        description: "Bosun, also called as boatswain, is an unlicensed member of the deck department and his duty is to supervise unlicensed deck crew of the merchant ship. The bosun is also task to do the planning, scheduling and assigning of work in the ship. His duties also depend on the type of ship, crewing and other aspect.",
        employmentBenefits: [],
        requiredTrainings: ["Certificate of Proficiency in Survival Craft and Rescue Boats"],
        otherQualifications: ["Well versed in marlinespike seamanship", "Has wide knowledge in the use of knots, hitches, bends, whipping, and splices for anchoring the vessel."],
        createdAt: new Date(),
        updatedAt: new Date(),
        isPublished: true,
        country: 'ph',
        state: 'NEW YORK'
    },{
        companyId: "",
        companyName: "",
        title: "Chief Cook",
        vesselType: "Cruise Ship",
        hasSalary: true,
        salary: {
            min: 1000,
            max: 2000
        },
        description: "Aboard ship, the steward department is responsible for the maintenance of living and eating quarters. Meal preparations are usually made a day ahead of time and cooking a meal for the crew takes from two to three hours. The men usually arise at 0530 to start breakfast and have rest periods between meals. In evenings, after supper, a cold meal of sandwiches and coffee is prepared for the night watch.",
        employmentBenefits: [],
        requiredTrainings: ["Navigational Watch Rating Certificate"],
        otherQualifications: [],
        createdAt: new Date(),
        updatedAt: new Date(),
        isPublished: true,
        country: 'jp',
        state: 'TOKYO'
    },{
        companyId: "",
        companyName: "",
        title: "Chief Engineer",
        vesselType: "Oil Tanker",
        hasSalary: true,
        salary: {
            min: 1000,
            max: 2000
        },
        description: "In charge of the engineering department and responsible to the Master for its efficient operation. Has overall control and decision making powers for the engineering department. Responsible for ensuring that all planned mechanical and electrical maintenance takes place. Co-ordinates operations with shore-side port engineer.",
        employmentBenefits: [],
        requiredTrainings: ["STCW III/2"],
        otherQualifications: [],
        createdAt: new Date(),
        updatedAt: new Date(),
        isPublished: true,
        country: 'ar',
        state: 'RIZAL'
    },{
        companyId: "",
        companyName: "",
        title: "2nd Engineer",
        vesselType: "General Cargo",
        hasSalary: true,
        salary: {
            min: 1000,
            max: 2000
        },
        description: "Directly responsible to the Chief Engineer Officer. Responsible for the management of the engine room and the engine room maintenance team. Oversees the training of engineers. Has watch-keeping engineering responsibilities.",
        employmentBenefits: [],
        requiredTrainings: ["STCW III/2"],
        otherQualifications: [],
        createdAt: new Date(),
        updatedAt: new Date(),
        isPublished: true,
        country: 'us',
        state: 'TOKYO'
    },{
        companyId: "",
        companyName: "",
        title: "2nd Engineer",
        vesselType: "General Cargo",
        hasSalary: true,
        salary: {
            min: 1000,
            max: 2000
        },
        description: "Directly responsible to the Chief Engineer Officer. Responsible for the management of the engine room and the engine room maintenance team. Oversees the training of engineers. Has watch-keeping engineering responsibilities.",
        employmentBenefits: [],
        requiredTrainings: ["STCW III/2"],
        otherQualifications: [],
        createdAt: new Date(),
        updatedAt: new Date(),
        isPublished: true,
        country: 'kr',
        state: 'NEW YORK'
    }];
}
