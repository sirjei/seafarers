Seafarers.Resources.Vessels = new Vessels();

function Vessels() {
  return {
    getList: getList
  }

  function getList() {
    return [
      "Bulk Carrier",
      "Car Carrier",
      "Oil Tanker",
      "Chemical Tanker",
      "VLCC",
      "LPG Carrier",
      "Container",
      "Dry Cargo",
      "Reefer",
      "Reefer container",
      "RORO",
      "OBO",
      "Multi-Purpose Vessel",
      "Cruise Ship",
      "Offshore Vessel",
      "Coastal Vessel",
      "TUG",
      "Dredger",
      "General Cargo",
      "Product Tanker",
      "Survey Vessel",
      "Wood/Log Carrier",
      "Fishing Vessel",
      "LNG Carrier",
      "Aframax Tanker",
      "Crude Oil Tanker",
      "D P Vessel",
      "FPSO"
    ];
  }
}
