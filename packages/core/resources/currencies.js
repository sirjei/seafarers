Seafarers.Resources.Currencies = new CurrencyResource();

function CurrencyResource () {
	return {
		getList: getList
	};

   function getList () {
      return [
      'dollar',
      'euro',
      'lira',
      'pound',
      'ruble',
      'rupee',
      'won',
      'yen'
      ];
   }
}
