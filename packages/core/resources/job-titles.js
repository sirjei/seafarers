Seafarers.Resources.JobTitles = new JobTitles();

function JobTitles() {
  return {
    getList: getList
  }

  function getList() {
    return [{
        order: 1,
        category: "Deck Department",
        name: "Master",
      },{
        order: 2,
        category: "Deck Department",
        name: "Chief Officer",
      },{
        order: 3,
        category: "Deck Department",
        name: "2nd Officer",
      },{
        order: 4,
        category: "Deck Department",
        name: "3rd Officer",
      },{
        order: 5,
        category: "Deck Department",
        name: "Radio Officer",
      },{
        order: 6,
        category: "Deck Department",
        name: "Deck Cadet",
      },{
        order: 7,
        category: "Deck Department",
        name: "Trainee Cadet",
      },{
        order: 8,
        category: "Deck Department",
        name: "Bosun",
      },{
        order: 9,
        category: "Deck Department",
        name: "Deck Fitter",
      },{
        order: 10,
        category: "Deck Department",
        name: "AB",
      },{
        order: 11,
        category: "Deck Department",
        name: "OS",
      },{
        order: 12,
        category: "Deck Department",
        name: "GP",
      },{
        order: 13,
        category: "Deck Department",
        name: "Crane Operator",
      },{
        order: 14,
        category: "Deck Department",
        name: "Junior Officer",
      },{
        order: 15,
        category: "Deck Department",
        name: "Junior Officer",
      },
      ////// Engineering
      {
        order: 1,
        category: "Engineering Department",
        name: "Chief Engineer",
      },{
        order: 2,
        category: "Engineering Department",
        name: "2nd Engineer",
      },{
        order: 3,
        category: "Engineering Department",
        name: "3rd Engineer",
      },{
        order: 4,
        category: "Engineering Department",
        name:"4th Engineer",
      },{
        order: 5,
        category: "Engineering Department",
        name:"5th Engineer",
      },{
        order: 6,
        category: "Engineering Department",
        name:"Electrical Engineer",
      },{
        order: 7,
        category: "Engineering Department",
        name:"Electrical Officer",
      },{
        order: 8,
        category: "Engineering Department",
        name:"Electro Technical Officer",
      },{
        order: 9,
        category: "Engineering Department",
        name:"Junior Engineer",
      },{
        order: 10,
        category: "Engineering Department",
        name:"Asst. Electrical Officer",
      },{
        order: 11,
        category: "Engineering Department",
        name:"Trainee Engineer",
      },{
        order: 12,
        category: "Engineering Department",
        name:"Reefer Engineer",
      },{
        order: 13,
        category: "Engineering Department",
        name:"Reefer Mechanic",
      },{
        order: 14,
        category: "Engineering Department",
        name:"Gas Engineer",
      },{
        order: 15,
        category: "Engineering Department",
        name:"Engine Fitter",
      },{
        order: 16,
        category: "Engineering Department",
        name:"Motorman",
      },{
        order: 17,
        category: "Engineering Department",
        name:"Wiper",
      },{
        order: 18,
        category: "Engineering Department",
        name:"Travel Fitter",
      },{
        order: 19,
        category: "Engineering Department",
        name:"Travel Wiper",
      },{
        order: 20,
        category: "Engineering Department",
        name:"Pielstik Engineer",
      },
      ///// steward
      {
        order: 1,
        category: "Steward Department",
        name: "Hotel Manager",
      },{
        order: 2,
        category: "Steward Department",
        name: "Purser",
      },{
        order: 3,
        category: "Steward Department",
        name: "Social Director",
      },{
        order: 4,
        category: "Steward Department",
        name: "Sports Director",
      },{
        order: 5,
        category: "Steward Department",
        name: "Chef",
      },{
        order: 6,
        category: "Steward Department",
        name: "Souse Chef",
      },{
        order: 7,
        category: "Steward Department",
        name: "Chief Cook",
      },{
        order: 8,
        category: "Steward Department",
        name: "2nd Cook",
      },{
        order: 9,
        category: "Steward Department",
        name: "Chief Steward",
      },{
        order: 10,
        category: "Steward Department",
        name: "Steward",
      },{
        order: 11,
        category: "Steward Department",
        name: "Cabin Attendant",
      },{
        order: 12,
        category: "Steward Department",
        name: "Bar Tender",
      },{
        order: 13,
        category: "Steward Department",
        name: "Musician",
      },{
        order: 14,
        category: "Steward Department",
        name: "Laundry Men",
      },{
        order: 15,
        category: "Steward Department",
        name: "Security Guard",
      },{
        order: 16,
        category: "Steward Department",
        name: "Safety Officer",
      }
    ];
  }
}
