Seafarers.Resources.Trainings = new Trainings();

function Trainings() {
  return {
    getList: getList
  }

  function getList() {
    return [
      "Basic Safety Course w/ PSSR",
      "Deck watchkeeping",
      "Basic Firefigthing",
      "Engine watchkeeping",
      "Personal Survival Technique",
      "Messman Course",
      "Elementary First Aid",
      "Catering Mgmt/Culinary Cert. (For Cook/Stwd)",
      "Personal Safety and Social Responsibility",
      "PADAMS",
      "Prof. in Survival Craft and Rescue Boat (PSC)",
      "MARPOL I",
      "Advanced Firefighting",
      "MARPOL II",
      "Medical Emergency-First Aid (MEFA)",
      "Ship Security Awareness (SSA)",
      "Medical Care (MECA)",
      "Shorebased Fire Fighting (SBFF)",
      "Automatic Radar Planning (ARPA)",
      "General Tanker Familiarization (GTF)",
      "Radar Observer Plotting (ROP)",
      "Specialized Training on Oil Tankers",
      "Radar Simulator Course (RSC)",
      "HAZMAT",
      "Ship Simulator and Bridge Teamwork (SSBT)",
      "Dangerous, Hazardous",
      "Bridge Resource Management (BRM)",
      "Engine Resource Management (ERM)",
      "Global Maritime Distress and Safety System (GMDSS)",
      "Auxiliary Machinery System",
      "INMARSAT",
      "Control Engineering",
      "Ship Restricted Radio Telephone Operators Course (SRROC)",
      "Hydraulics/Pneumatics",
      "Shift Security Officer (SSO)",
      "Smoke Diving",
      "Electronic Chart Display (ECDIS)",
      "Crude Oil Washing",
      "Cargo Handling and Care of Cargo",
      "Maritime Law for Ships Officers",
      "Collision Avoidance",
      "Maritime Refregeration and Air Conditioning",
    ];
  }
}
