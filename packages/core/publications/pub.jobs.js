var jobNotFound = new Meteor.Error('job-not-found',
    'Specific job to subscribe is not found.');

Meteor.publish('specificJob', function (id) {
    var jobs = Seafarers.Collections.Jobs.find(id);

    if (jobs.fetch().length)
        return jobs;
    else
        throw jobNotFound;
});

Meteor.publish('jobs', function () {
    return Seafarers.Collections.Jobs.find();
});

Meteor.publishComposite('singleJobWithCompany',  function(jobId) {
    return {
        find: function() {
            var job = Seafarers.Collections.Jobs.find(jobId);

            if (job.fetch().length)
                return job;
            else
                throw jobNotFound;
        },
        children: [
            {
                find: function(job) {
                    var company = Seafarers.Collections.Companies.find(job.companyId);

                    if (company.fetch().length)
                        return company;
                    else
                        throw new Meteor.Error('job\'s-company-not-found',
                            'Specific job to subscribe is not found.');
                }
            }
        ]
    };
});
