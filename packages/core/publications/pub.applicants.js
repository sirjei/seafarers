Meteor.publish('specificApplicant', function (selector) {
    var applicant = Seafarers.Collections.Applicants.find(selector);
    
    if (applicant.fetch().length)
        return applicant;
    else
        throw new Meteor.Error('applicant-not-found', 'Specific applicant to subscribe is not found.');
});

Meteor.publish('Applicant', function () {
    return Seafarers.Collections.Applicants.find();
});
