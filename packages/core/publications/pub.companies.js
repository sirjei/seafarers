Meteor.publish('specificCompany', function (selector) {
    var company = Seafarers.Collections.Companies.find(selector);

    if (company.fetch().length)
        return company;
    else
        throw new Meteor.Error('company-not-found', 'Specific company to subscribe is not found.');
});

Meteor.publish('Companies', function () {
    return Seafarers.Collections.Companies.find();
});
