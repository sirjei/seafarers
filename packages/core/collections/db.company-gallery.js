var db = Seafarers.Collections;

var filters = {
  maxSize: 2097152, //in bytes, 2mb
  allow: {
    contentTypes: ['image/*']
  },
  onInvalid: function (message) {
    if (Meteor.isServer)
      throw new Meteor.Error(403, message);
  }
};

var collectionName = "companyGallery",
    storeName = "gallery";

if (Meteor.isServer) {

  db.CompanyGallery = new FS.Collection(collectionName, {
    stores: [
      new FS.Store.FileSystem(storeName, {
          path: process.env.PWD + '/.uploads/gallery',
          transformWrite: function(fileObj, readStream, writeStream) {
            // Transform the image into a 140x140px image
            gm(readStream, fileObj.name()).resize('140', '140', "!").stream().pipe(writeStream);
          }
      })
    ],
    filter: filters
  });

  db.CompanyGallery.allow({
      insert: function(userId, doc) {
          return true;
      },
      update: function(userId, doc, fieldNames, modifier) {
          return true;
      },
      remove: function() {
          return true;
      },
      download: function() {
          return true;
      }
  });
} else {
  db.CompanyGallery = new FS.Collection(collectionName, {
    stores: [new FS.Store.FileSystem(storeName)]
  });
}



db.CompanyGallery.on('uploaded', function (fileObj) {
  //console.log("uploaded");
});
