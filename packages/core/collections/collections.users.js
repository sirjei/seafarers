Meteor.users.attachSchema(Seafarers.Schemas.Users);

Meteor.users.deny({
    update: function (userId, doc, fields, modifier) {
        _.each(getAllProps(modifier), function (element) { // deny attempt to change 'profile.type' field
            if (element.indexOf('type') !== -1)
                throw new Meteor.Error('access-denied', 'Only super admins may only modify the type field.');
        });

        return userId && doc._id === userId;

        function getAllProps(object) {
            var allProps = [];

            getProps(object);

            function getProps(object) {
                _.each(object, function (element, index, list) {
                    allProps.push(index);
                    if (_.isObject(element))
                        return getProps(element);
                });
            }

            return allProps;
        }
    }
});
