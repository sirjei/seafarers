Seafarers.Collections.Jobs = new Meteor.Collection("jobs");

Seafarers.Collections.Jobs.allow({
  insert: function (userId, doc) {
    return (userId && doc.authorId === userId);
  },
  update: function (userId, doc, fields, modifier) {
    return doc.authorId === userId;
  },
  remove: function (userId, doc) {
    return doc.authorId === userId;
  },
});
