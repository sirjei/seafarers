Seafarers.Collections.Companies = new Meteor.Collection("companies");

Seafarers.Collections.Companies.allow({
  insert: function (userId, doc) {
    return doc.userId.indexOf(userId) !== -1;
  },
  update: function (userId, doc, fields, modifier) {
    return doc.userId.indexOf(userId) !== -1;
  },
  remove: function (userId, doc) {
    return doc.userId.indexOf(userId) !== -1;
  }
});
