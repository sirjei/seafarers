Seafarers.Collections.Applicants = new Meteor.Collection("applicants");

Seafarers.Collections.Applicants.allow({
  insert: function (userId, doc) {
    return doc.userId === userId;
  },
  update: function (userId, doc, fields, modifier) {
    return doc.userId === userId;
  },
  remove: function (userId, doc) {
    return doc.userId === userId;
  }
});
