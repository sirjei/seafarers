Seafarers.Helpers.Common = {};

Seafarers.Helpers.Common.formatDate = function(date, option) {
    if(date)
        return moment(date).format(option);
};


Seafarers.Helpers.Common.trimObject = function (object) {
    _.each(object, function(value, key) {
        if(!value)
            delete object[key];
    });

    return object;
};

Seafarers.Helpers.Common.getProp = function (obj, prop) {

    if (typeof obj === 'undefined') return false;

    var _index = prop.indexOf('.');

    if (_index > -1) return Seafarers.Helpers.Common.getProp(obj[prop.substring(0, _index)], prop.substr(_index+1));

    return obj[prop];

};

Seafarers.Helpers.Common.setProp = function (obj, prop, value) {
    if (typeof prop === "string")
        prop = prop.split(".");

    if (prop.length > 1) {
        var e = prop.shift();
        Seafarers.Helpers.Common.setProp(obj[e] =
                 Object.prototype.toString.call(obj[e]) === "[object Object]"
                 ? obj[e]
                 : {},
               prop,
               value);
    } else
        obj[prop[0]] = value;
};

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}
