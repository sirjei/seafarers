Seafarers.Helpers.Router = {};

Seafarers.Helpers.Router.requireLogin = function (path, next) {
    var redirectPath = !Meteor.user() ? "/sign-in" : null;
    next(redirectPath);
};
