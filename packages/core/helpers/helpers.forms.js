Seafarers.Helpers.Forms = {};

jQuery.fn.extend({
    numberInput: function() {
        this.keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40))
            // let it happen, don't do anything
                return;

            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105))
                e.preventDefault();
        });

        return this;
    },
    maxInputLength: function(maxLength) {
        this.keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40))
            // let it happen, don't do anything
                return;

            if ($(this).val().length >= maxLength)
                e.preventDefault();
        });

        return this;
    },
	setSelected: function(value) {
		var matchedOption = this.find('[value=' + value + ']');
        matchedOption.prop('selected', true);
	}
});

Seafarers.Helpers.Forms.partnerFields = function (fieldCouples, current, update) {
    var field = current.replace(/-/g, '.');

    _.each(fieldCouples, function (set) {
        checkEachOther(set.one, set.two);
    });

    function checkEachOther(inputOne, inputTwo) {
        checkOther(inputOne, inputTwo);
        checkOther(inputTwo, inputOne);
    }

    function checkOther(inputOne, inputTwo) {
        if (current.indexOf(inputOne) !== -1)
            // if partner has error, validate and insert
            if ($('[name=' + current.replace(inputOne, inputTwo) + ']').parent().hasClass('error')) {
                var partnersField = field.replace(inputOne, inputTwo);
                update(partnersField);
            }
    }
};

Seafarers.Helpers.Forms.validateEachOther = function (arg) {
    var inputOne = arg.inputOne,
    inputTwo = arg.inputTwo,
    current = arg.current,
    context = arg.context,
    data = arg.data,
    field = current.replace(/-/g, '.');
    
    checkOther(inputOne, inputTwo);
    checkOther(inputTwo, inputOne);

    function checkOther(inputOne, inputTwo) {
        if (current.indexOf(inputOne) !== -1)
            // if partner has error, validate and insert
            if ($('[name=' + current.replace(inputOne, inputTwo) + ']').parent().hasClass('error')) {
                var partnersField = field.replace(inputOne, inputTwo);
                context.validate(data, partnersField);
            }
    }
};
