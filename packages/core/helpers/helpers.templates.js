UI.registerHelper('formatDate', function(date, option) {
    return Seafarers.Helpers.Common.formatDate(date, option);
});

UI.registerHelper('upperCase', function(str) {
    if (str)
        return str.toUpperCase();
    else
        return '(unspecified)';
});
