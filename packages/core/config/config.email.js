Meteor.startup(function () {
  var smtp = {
    username: 'gamo.jerome@gmail.com',
    password: 'donttrythatjedimindshitonme',
    server:   'smtp.gmail.com',
    port: 465
  }

  process.env.MAIL_URL = 'smtp://' + encodeURIComponent(smtp.username) + ':' + encodeURIComponent(smtp.password) + '@' + encodeURIComponent(smtp.server) + ':' + smtp.port;
});

Accounts.emailTemplates.siteName = "Seafarers";
Accounts.emailTemplates.from = "Seafarers Admin <accounts@example.com>";

/*
* Reset Password Template
*/
Accounts.emailTemplates.resetPassword.subject = function (user) {
    return "Forgot Password";
};

Accounts.emailTemplates.resetPassword.html = function (user, url) {

    var token = getToken(url),
        resetUrl = Meteor.absoluteUrl() + "reset-password/" + token;

    SSR.compileTemplate('emailForgotPassword', Assets.getText('assets/email/email.forgot-password.html'));

    return SSR.render("emailForgotPassword", {url: resetUrl, name: "Jerome"});
};

/*
* Enroll Password Template
*/
Accounts.emailTemplates.enrollAccount.subject = function (user) {
    return "Welcome to Seafarer's Jobsite";
};

Accounts.emailTemplates.enrollAccount.html = function (user, url) {

    var token = getToken(url),
        resetUrl = Meteor.absoluteUrl() + "enroll-account/" + token;

    SSR.compileTemplate('emailEnrollAccount', Assets.getText('assets/email/email.enroll-account.html'));

    return SSR.render("emailEnrollAccount", {url: resetUrl, name: "Jerome"});
};

function getToken(url) {
    var urlArr = url.split("/");
    return urlArr[urlArr.length - 1];
}
