Seafarers.Core.Factories.Steps = Steps;

function Steps(steps, collection, checkMethod) {
    var isEnabled, isCompleted, nextAnticipated, isChanged, selector,
    farthestStep,
    stepTracker = new Tracker.Dependency();

    steps =  _.map(steps, function (element, index, list) {
        isEnabled = element.isEnabled;
        isCompleted = element.isCompleted;

        if (!_.isBoolean(isEnabled))
            isEnabled = false;

        if (!_.isBoolean(isCompleted))
            isCompleted = false;

        return _.extend(element, {
            isEnabled: isEnabled,
            isCompleted: isCompleted
        });
    })

    this.getAll = getAll;
    this.getOne = getOne;
    this.getNext = getNext;
    this.disableNextSteps = disableNextSteps;
    this.getFarthestStep = getFarthestStep;
    this.saveLastStep = saveLastStep;
    this.check = check;
    this.firstStep = steps[0].name;
    this.lastStep = steps[steps.length - 1].name;

    function getAll() {
        stepTracker.depend();

        return steps;
    }

    function getOne(stepName) {
        stepTracker.depend();

        return _.find(steps, function (element) {
            return element.name === stepName;
         });
    }

    function getNext(stepName) {
        stepTracker.depend();

        nextAnticipated = false;

        return _.find(steps, function (element) {
            if (nextAnticipated)
                return true;

            if (element.name === stepName)
                nextAnticipated = true;
         });
    }

    function enableNext(currentStepName) {
        nextAnticipated  = false;
        isChanged = false;

        mappedSteps = _.map(steps, function (element) {
            if (!nextAnticipated) {
                if (element.name === currentStepName)
                    nextAnticipated = true;

                return element;
            } else if (nextAnticipated) {
                nextAnticipated = false;

                if (!element.isEnabled) {
                    element.isEnabled = true;
                    isChanged = true;
                }

                return element;
            }
        });

        if (isChanged) {
            steps = mappedSteps;
            stepTracker.changed();
        }
    }

    function enableOnly(stepNames) {
        if (_.isArray(stepNames)) {
            steps = _.map(steps, function (element, index, list) {
                if ( // if matched and not set to true yet
                    _.indexOf(stepNames, element.name) !== -1 &&
                    !element.isEnabled
                )
                    element.isEnabled = true;
                else if ( // if not matched but already set to true
                    _.indexOf(stepNames, element.name) === -1 &&
                    element.isEnabled
                )
                    element.isEnabled = false;

                return element;
            });

            stepTracker.changed();
        } else
            throw new Meteor.Error(500, "stepNames argument must be an array");
    }

    function disableNextSteps(currentStepName) {
        nextAnticipated = false;

        mappedSteps = _.map(steps, function (element) {
            if (!nextAnticipated) {
                if (element.name === currentStepName)
                    nextAnticipated = true;

                return element;
            } else if (nextAnticipated) {
                if (element.isEnabled) {
                    element.isEnabled = false;

                    if (!isChanged)
                        isChanged = true;
                }

                return element;
            }
        });

        if (isChanged) {
            steps = mappedSteps;
            stepTracker.changed();
        }
    }

    function getFarthestStep() {
        check();

        _.find(steps, function (step) {
            if (step.isEnabled)
                farthestStep = step.name;
            else
                return true;
        });

        return farthestStep;
    }


    function setCompleted(stepName) {
        steps = _.map(steps, function (element, index, list) {
            if ( // if matched and not set to true yet
                stepName === element.name &&
                !element.isCompleted
            )
                element.isCompleted = true;

            return element;
        });

        stepTracker.changed();
    }


    function setIncomplete(stepName) {
        steps = _.map(steps, function (element, index, list) {
            if ( // if matched and not set to true yet
                stepName === element.name &&
                element.isCompleted
            )
                element.isCompleted = false;

            return element;
        });

        stepTracker.changed();
    }

    function check() {
        checkMethod({
            setCompleted: setCompleted,
            setIncomplete: setIncomplete,
            enableNext: enableNext,
            enableOnly: enableOnly,
            disableNextSteps: disableNextSteps
        });

        stepTracker.changed();
    }

    function saveLastStep(currentStep, doc) {
        if (doc.currentStep !== currentStep)
            collection.update(doc._id, {
                $set: {lastStep: currentStep}
            });
    }
}
