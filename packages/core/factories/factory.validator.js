Seafarers.Core.Factories.Validator = Validator;

function Validator(schema,contextName) {

  var context = schema.namedContext(contextName);

  this.validate = function (fields, options) {
    schema.clean(fields, {isModifier: options && options.modifier});
    context.validate(fields, options);
    return context.isValid();
  };

  this.validateOne = function (obj, field, options) {
    schema.clean(obj, {isModifier: options && options.modifier});
    context.validateOne(obj, field, options);
    return context.keyIsInvalid(field);
  };

  this.isValid = function () {
    return context.isValid();
  }

  this.hasError = function (field) {
    return context.keyIsInvalid(field);
  };

  this.getErrorMessage = function (field) {
    return context.keyErrorMessage(field);
  };

  this.reset = function () {
    context.resetValidation();
  };

  this.getInvalidKeys = function () {
    return context.invalidKeys();
  }

  this.addInvalidKeys = function (errors) {
    return context.addInvalidKeys(errors);
  }
}
