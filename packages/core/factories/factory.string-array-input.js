Seafarers.Core.Factories.StrArrInput = StrArrInput;

function StrArrInput(arg) {
    var _strings = arg.strings,
    _collection = arg.collection,
    _documentId = arg.documentId,
    _field = arg.field,
    _tracker = new Tracker.Dependency(),
    modifier;

    this.get = function () {
        _tracker.depend();

        return _strings;
    };

    this.add = function (training) {
        modifier = {};

        training = training.trim();
        modifier[_field] = training;

        if (_strings.indexOf(training) === -1) {
            _strings.push(training);

            _collection.update(_documentId, {
                $addToSet: modifier
            }, function (error, affected) {
                if (error && !affected) {
                    toastr.error('Sorry, saving updates failed, please try again.', 'Server Error!');
                    _strings = _.reject(training, function (value) {
                        return value === training;
                    });
                }
            });

            _tracker.changed();
        } else
            toastr.error(arg.name + ' already added', 'Invalid!');
    };

    this.remove = function (training) {
        modifier = {};

        modifier[_field] = training;

        _strings = _.reject(_strings, function (value) {
            return value === training;
        });

        _collection.update(_documentId, {
            $pull: modifier
        }, function (error, affected) {
            if (error && !affected) {
                _strings.push(training);
                toastr.error('Sorry, saving updates failed, please try again.', 'Server Error!');
            }

        });

        _tracker.changed();
    };
}
