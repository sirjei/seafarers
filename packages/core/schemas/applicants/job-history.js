 // attached to collection, with custom validation of requiredUnlessDraft
 // in order to push incomplete documents
 var schemas = Seafarers.Schemas;

schemas.DBJobHistories = new SimpleSchema([
    schemas.lastStep,
    {
        jobHistories: {
            type: [new SimpleSchema({
                _id: {
                    type: String
                },
                isDraft: {
                    type: Boolean,
                    autoValue: function () {
                        return this && _.isBoolean(this) ? this : true;
                    }
                },
                agency: {
                    optional: true,
                    custom: function () {
                        return SchemaHelpers.requiredUnlessDraft(this);
                    },
                    type: String,
                    autoValue: function () {
                        return SchemaHelpers.setToLowerCase(this);
                    },
                },
                rank: {
                    optional: true,
                    custom: function () {
                        return SchemaHelpers.requiredUnlessDraft(this);
                    },
                    type: String,
                    autoValue: function () {
                        return SchemaHelpers.setToLowerCase(this);
                    },
                },
                vesselType: {
                    optional: true,
                    custom: function () {
                        return SchemaHelpers.requiredUnlessDraft(this);
                    },
                    type: String,
                    autoValue: function () {
                        return SchemaHelpers.setToLowerCase(this);
                    },
                },
                vesselName: {
                    type: String,
                    optional: true,
                    autoValue: function () {
                        return SchemaHelpers.setToLowerCase(this);
                    }
                },
                salary: {
                    type: new SimpleSchema({
                        min: {
                            type: Number,
                            label: "Minimum salary",
                            min: 0,
                            max: 9999999,
                            optional: true,
                            custom: function () {
                                return SchemaHelpers.greaterThan('max', this) ||
                                    SchemaHelpers.requiredUnlessDraft(this) ||
                                    SchemaHelpers.requiredWith('max', this);
                            }
                        },
                        max: {
                            type: Number,
                            label: "Maximum salary",
                            min: 0,
                            max: 9999999,
                            optional: true,
                            custom: function () {
                                return SchemaHelpers.lessThan('min', this) ||
                                    SchemaHelpers.requiredUnlessDraft(this) ||
                                    SchemaHelpers.requiredWith('min', this);
                            }
                        }
                    }),
                    optional: true
                },
                signIn: {
                    optional: true,
                    type: Date,
                    custom: function () {
                        return SchemaHelpers.greaterThanOrEqual('signOff', this, true) ||
                            SchemaHelpers.requiredUnlessDraft(this) ||
                            SchemaHelpers.requiredWith('signOff', this);
                    }
                },
                signOff: {
                    optional: true,
                    type: Date,
                    custom: function () {
                        return SchemaHelpers.lessThanOrEqual('signIn', this, true) ||
                            SchemaHelpers.requiredUnlessDraft(this) ||
                            SchemaHelpers.requiredWith('signIn', this);
                    }
                }
            })]
        }
}]);

schemas.JobHistory = new SimpleSchema({
    isDraft: {
        type: Boolean,
        autoValue: function () {
            return this && _.isBoolean(this) ? this : true;
        }
    },
    agency: {
        type: String,
        autoValue: function () {
            return SchemaHelpers.setToLowerCase(this);
        }
    },
    rank: {
        type: String,
        autoValue: function () {
            return SchemaHelpers.setToLowerCase(this);
        }
    },
    vesselType: {
        type: String,
        autoValue: function () {
            return SchemaHelpers.setToLowerCase(this);
        }
    },
    vesselName: {
        type: String,
        optional: true
    },
    salary: {
        type: schemas.SalaryRange,
        optional: true
    },
    signIn: {
        type: Date,
        custom: function () {
            return SchemaHelpers.greaterThanOrEqual('signOff', this, true) ||
                SchemaHelpers.requiredWith('signOff', this);
        }
    },
    signOff: {
        type: Date,
        custom: function () {
            return SchemaHelpers.lessThanOrEqual('signIn', this, true) ||
                SchemaHelpers.requiredWith('signIn', this);
        }
    }
});
