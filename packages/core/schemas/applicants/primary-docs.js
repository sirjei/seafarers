var schemas = Seafarers.Schemas,
documentsSchema = new SimpleSchema({
    number: {
        type: String,
        autoValue: function () {
            return SchemaHelpers.setToLowerCase(this);
        }
    },
    dateIssued: {
        type: Date,
        custom: function () {
            return SchemaHelpers.requiredWith('validUntil', this) ||
                SchemaHelpers.greaterThanOrEqual('validUntil', this, true);
        }
    },
    validUntil: {
        type: Date,
        custom: function () {
            return SchemaHelpers.requiredWith('dateIssued', this) ||
                SchemaHelpers.lessThanOrEqual('dateIssued', this, true);
        }
    }
}),
primaryDocsSchema = new SimpleSchema([
    schemas.lastStep,
    {
        rank: {
            type: String,
            autoValue: function () {
                return SchemaHelpers.setToLowerCase(this);
            }
        },
        vesselType: {
            type: String,
            optional: true,
            autoValue: function () {
                return SchemaHelpers.setToLowerCase(this);
            }
        },
        salary: {
            type: schemas.SalaryRange,
            optional: true
        },
        primaryDocs: {
            type: new SimpleSchema({
                passport: {
                    type: documentsSchema
                },
                visa: {
                    type: documentsSchema
                },
                seamansBook: {
                    type: documentsSchema
                }
            })
        }
    }
]);

schemas.DBPrimaryDocs = primaryDocsSchema;

schemas.PrimaryDocs = primaryDocsSchema.pick([
    'rank',
    'vesselType',
    'salary',
    'salary.min',
    'salary.max',
    'primaryDocs.passport',
    'primaryDocs.passport.dateIssued',
    'primaryDocs.passport.validUntil',
    'primaryDocs.passport.number',
    'primaryDocs.visa',
    'primaryDocs.visa.dateIssued',
    'primaryDocs.visa.validUntil',
    'primaryDocs.visa.number',
    'primaryDocs.seamansBook',
    'primaryDocs.seamansBook.dateIssued',
    'primaryDocs.seamansBook.validUntil',
    'primaryDocs.seamansBook.number',
]);
