var schemas = Seafarers.Schemas,
basicInfoSchema = new SimpleSchema([
    schemas.lastStep,
    schemas.Location,
    {
        userId: {
            type: String
        },
        gender: {
            type: String,
            allowedValues: ['male', 'female']
        },
        birthday: {
            type: Date
        },
        contactNo: {
            type: String,
            optional: true,
            autoValue: function () {
                return SchemaHelpers.setToLowerCase(this);
            }
        }
    }
]);

schemas.DBBasicInfo = basicInfoSchema;

schemas.BasicInfo = basicInfoSchema.pick([
    'address',
    'country',
    'state',
    'city',
    'zipCode',
    'gender',
    'birthday',
    'contactNo'
]);
