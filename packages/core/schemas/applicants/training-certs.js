var schemas = Seafarers.Schemas,
trainingCertsSchema = new SimpleSchema([
    schemas.lastStep,
    {
        trainingCerts: {
            type: [String]
        }
    }
 ]);

schemas.DBTrainingCerts = trainingCertsSchema;

schemas.TrainingCerts = trainingCertsSchema.pick([
    'trainingCerts',
    'trainingCerts.$'
]);
