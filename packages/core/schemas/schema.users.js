var schemas = Seafarers.Schemas;

schemas.UsersName = new SimpleSchema({
    firstName: {
        type: String,
        label: "First Name",
        autoValue: function () {
            return SchemaHelpers.setToLowerCase(this);
        }
    },
    lastName: {
        type: String,
        label: "Last Name",
        autoValue: function () {
            return SchemaHelpers.setToLowerCase(this);
        }
    }
});

schemas.UsersProfile = new SimpleSchema([
    schemas.UsersName,
    {
        type: {
            type: String,
            allowedValues: ['company', 'applicant']
        }
    }
]);

schemas.Users = new SimpleSchema([
    {
        emails: {
            type: [Object],
            optional: true
        },
        "emails.$.address": {
            type: String,
            regEx: SimpleSchema.RegEx.Email
        },
        "emails.$.verified": {
            type: Boolean
        },
        createdAt: {
            type: Date
        },
        services: {
            type: Object,
            optional: true,
            blackbox: true
        },
        profile: {
            type: schemas.UsersProfile
        }
    }
]);

schemas.CreateUser = new SimpleSchema([
    schemas.UsersName,
    {
        email: {
            type: String,
            regEx: SimpleSchema.RegEx.Email
        }
    }
]);

schemas.Login = new SimpleSchema({
        email: {
            type: String,
            regEx: SimpleSchema.RegEx.Email
        },
        password: {
            type: String,
            label: "Password",
            min: 8
        }
});

schemas.Passwords = new SimpleSchema({
    password: {
        type: String,
        label: "Password",
        min: 8
    },
    confirmPassword: {
        type: String,
        label: "Confirm password",
        optional: true,
        custom: function () {
        if (this.field('password').isSet && (this.value !== this.field('password').value))
            return "passwordMismatch";
        }
    }
});
