SimpleSchema.messages({
    "unique": "[label] already exist",
    "passwordMismatch": "Passwords do not match",
    "lessThanMaxSalary": "Must be less than the maximum salary",
    tooLess: "[label] must be greater than the minimum",
    tooGreat: "[label] must be less than the maximum",
    "tooLess-dateIssued": "Must be later than the date issued",
    "tooGreat-validUntil": "Must be earlier than the expiration date",
    "tooLess-signIn": "Must be later than the sign in date",
    "tooGreat-signOff": "Must be earlier than the sign off data",
    alreadyExists: "[label] already exists",
    userIsCompany: "Company is not allowed",
    emailNotFound: "Email does not exist",
    incorrectPassword: "Incorrect password!"
});
