var schemas = Seafarers.Schemas,
jobsSchema = new SimpleSchema([
    schemas.Timestamps,
    {
        companyId: {
            type: String,
        },
        companyName: {
            type: String,
            autoValue: function () {
                return SchemaHelpers.setToLowerCase(this);
            }
        },
        authorId: {
            type: String
        },
        applications: {
            type: new SimpleSchema({
                userId: {
                    type: String
                },
                isSent: {
                    type: Boolean
                }
            }),
            optional: true
        },
        isPublished: {
            type: Boolean,
            optional: true,
        },
        title: { //position or rank
            type: String,
            label: "Job Title",
            autoValue: function () {
                return SchemaHelpers.setToLowerCase(this);
            }
        },
        state: {
            type: String,
            autoValue: function () {
                return SchemaHelpers.setToLowerCase(this);
            }
        },
        country: {
            type: String,
            autoValue: function () {
                return SchemaHelpers.setToLowerCase(this);
            }
        },
        vesselType: {
            type: String,
            label: "Vessel Type",
            autoValue: function () {
                return SchemaHelpers.setToLowerCase(this);
            }
        },
        hasSalary: {
            type: Boolean,
            defaultValue: false
        },
        salary: {
            type: new SimpleSchema({
                max: {
                    type: Number,
                    label: "Maximum salary",
                    min: 0,
                    max: 9999999,
                    optional: true,
                    custom: function () {
                        if (this.field('hasSalary').value === true && !this.isSet)
                            return 'required';

                        return SchemaHelpers.lessThan('min', this);
                    }
                },
                min: {
                    type: Number,
                    label: "Minimum salary",
                    min: 0,
                    max: 9999999,
                    optional: true,
                    custom: function () {
                        if (this.field('hasSalary').value === true && !this.isSet)
                            return 'required';

                        return SchemaHelpers.greaterThan('max', this);
                    }
                }
            }),
            optional: true,
            custom: function () {
                if (this.siblingField('hasSalary').value === true && !this.isSet)
                    return 'required';
            }
        },
        description: {
            type: String,
            label: "Description",
            optional: true,
        },
        employmentBenefits: {
            type: [String],
            label: "Employment Benefits",
            optional: true
        },
        requiredTrainings: {
            type: [String],
            label: "Required Training",
            optional: true
        },
        otherQualifications: {
            type: [String],
            label: "Other Qualifications",
            optional: true
        }
    }
]);

schemas.JobOwner = jobsSchema.pick([
    'companyId',
    'companyName',
    'state',
    'country',
    'authorId',
    'createdAt',
    'updatedAt'
]);

schemas.Applications = jobsSchema.pick([
    'userId',
    'isSent',
]);

schemas.DBJobDetails = jobsSchema.pick([
    'isPublished',
    'title',
    'vesselType',
    'hasSalary',
    'salary',
    'salary.min',
    'salary.max',
    'description',
    'employmentBenefits',
    'employmentBenefits.$',
    'requiredTrainings',
    'requiredTrainings.$',
    'otherQualifications',
    'otherQualifications.$',
    'createdAt',
    'updatedAt'
]);

schemas.JobDetails = jobsSchema.pick([
    'title',
    'vesselType',
    'hasSalary',
    'salary',
    'salary.min',
    'salary.max',
    'description',
    'employmentBenefits',
    'employmentBenefits.$',
    'requiredTrainings',
    'requiredTrainings.$',
    'otherQualifications',
    'otherQualifications.$'
]);
