Seafarers.Schemas.Timestamps = new SimpleSchema({
    createdAt: {
        type: Date,
        defaultValue: new Date(),
        denyUpdate: true
    },
    updatedAt: {
        type: Date,
        defaultValue: new Date(),
    }
});
