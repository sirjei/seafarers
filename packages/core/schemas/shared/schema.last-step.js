Seafarers.Schemas.lastStep = new SimpleSchema({
    lastStep: {
        type: String,
        optional: true,
        custom: function () {
            var user = Meteor.user();

            // require for unverified and logged in user
            if (user && !user.emails[0].verified && !this.isSet)
               return 'required';
        }
    }
});
