Seafarers.Schemas.Location = new SimpleSchema({
    country: {
        type: String,
    },
    address: {
        type: String,
    },
    city: {
        type: String,
    },
    state: {
        type: String,
    },
    zipCode: {
        type: Number,
        regEx: SimpleSchema.RegEx.ZipCode
    }
});
