Seafarers.Schemas.Salary = new SimpleSchema({
  /*
  * Either salaryInfo or proposedSalary should be set
  */
  //if salary info is set.. save a description
  salaryInfo: {
    type: String,
    optional: true
  },
  proposedSalary: {
    type: Object,
    optional: true
  },
  //if proposedSalary is set.. require sub document
  'proposedSalary.min': {
    type: Number,
    label: "Minimum salary",
    minCount: 0,
    maxCount: 1000000,
  },
  'proposedSalary.max': {
    type: Number,
    label: "Maximum salary",
    minCount: 0,
    maxCount: 1000000,

  }
});

Seafarers.Schemas.SalaryRange = new SimpleSchema({
    max: {
        type: Number,
        label: "Maximum salary",
        min: 0,
        max: 9999999,
        optional: true,
        custom: function () {
            return SchemaHelpers.requiredWith('min', this) ||
                SchemaHelpers.lessThan('min', this);
        }
    },
    min: {
        type: Number,
        label: "Minimum salary",
        min: 0,
        max: 9999999,
        optional: true,
        custom: function () {
            return SchemaHelpers.requiredWith('max', this) ||
                SchemaHelpers.greaterThan('max', this);
        }
    }
});
