SchemaHelpers = {};

SchemaHelpers.setToLowerCase = function (field) {
    if (field.isSet && typeof field.value === "string") {
        return field.value.toLowerCase();
    }
};

SchemaHelpers.setToUpperCase = function (field) {
    if (field.isSet && typeof field.value === "string") {
        return field.value.toUpperCase();
    }
};

SchemaHelpers.greaterThan = function (otherField, field) {
    if (
        field.isSet &&
        field.siblingField(otherField).isSet &&
        field.value > field.siblingField(otherField).value
    )
        return 'tooGreat';
};

SchemaHelpers.lessThan = function (otherField, field) {
    if (
        field.isSet &&
        field.siblingField(otherField).isSet &&
        field.value < field.siblingField(otherField).value
    )
        return 'tooLess';
};

SchemaHelpers.greaterThanOrEqual = function (otherField, field, type) {
    if (
        field.isSet &&
        field.siblingField(otherField).isSet &&
        field.value >= field.siblingField(otherField).value
    )
        if (type)
            return 'tooGreat-' + otherField;
        else
            return 'tooless';
};

SchemaHelpers.lessThanOrEqual = function (otherField, field, type) {
    if (
        field.isSet &&
        field.siblingField(otherField).isSet &&
        field.value <= field.siblingField(otherField).value
    )
        if (type)
            return 'tooLess-' + otherField;
        else
            return 'tooless';
};

SchemaHelpers.requiredWith = function (otherField, field) {
    if (!field.isSet && field.siblingField(otherField).isSet)
        return 'required';
};

SchemaHelpers.requiredUnlessDraft = function (field) {
    if (!field.isSet && field.siblingField('isDraft').value !== true)
        return 'required';
    else if (!field.isSet && field.siblingField('isDraft').value === true)
        return true;
};
