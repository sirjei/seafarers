var schemas = Seafarers.Schemas,
companiesSchema = new SimpleSchema([
    schemas.lastStep,
    schemas.Location,
    schemas.Timestamps,
    {
        companyName: {
            type: String,
            label: "Your company name",
            autoValue: function () {
                return SchemaHelpers.setToLowerCase(this);
            }
        },
        userId: {
            type: [String],
            min: 1
        }
    }
]);

schemas.DBCompanies = companiesSchema;

schemas.Companies = companiesSchema.pick([
    'companyName',
    'address',
    'country',
    'state',
    'city',
    'zipCode',
    'createdAt',
    'updatedAt'
]);
