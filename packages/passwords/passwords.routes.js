

FlowRouter.route('/forgot-password', {
    action: function(params) {
      FlowLayout.render('layoutBlank',{mainContent: 'passwordForgot'});
    }
});

FlowRouter.route('/enroll-account/:token', {
    action: function(params) {
      FlowLayout.render('layoutBlank',{mainContent: 'passwordEnroll'});
    }
});

FlowRouter.route('/reset-password/:token', {
    action: function(params) {
      FlowLayout.render('layoutBlank',{mainContent: 'passwordReset'});
    }
});

FlowRouter.route('/forgot-password/:token', {
    action: function(params) {
      FlowLayout.render('layoutBlank',{mainContent: 'passwordReset'});
    }
});
