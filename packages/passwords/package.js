Package.describe({
    name: 'sj-app:passwords',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: '',
});

Package.onUse(function(api) {
    api.versionsFrom('1.1.0.2');

    api.use("accounts-password");
    api.use("meteorhacks:flow-router");
    api.use('tracker');
    api.use('aldeed:simple-schema');
    api.use('sj-app:core');

    api.addFiles([
        'namespace.js',
        'passwords.validations.js',
        'passwords.methods.js',
        'passwords.routes.js'
    ]);

    api.addFiles([
      //'passwords.reset.js'
  ], 'client');
});
