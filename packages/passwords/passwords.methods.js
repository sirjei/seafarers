// Write your package code here!
//
Meteor.methods({
    resetPass: function () {
        console.log("User ID", this.userId);

        if (!this.userId)
            throw new Meteor.Error(400,"Cannot process request");

        if (!this.isSimulation)
            Accounts.sendResetPasswordEmail(this.userId);

        return;
    }
});
