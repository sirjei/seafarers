
var Validator = Seafarers.Core.Factories.Validator;

var resetPasswordForm = Seafarers.Schemas.Users.pick([
  'password',
  'confirmPassword'
]);

Seafarers.Validations.Passwords.Reset = new Validator(resetPasswordForm,"resetPasswordForm");
Seafarers.Validations.Passwords.Enroll = new Validator(resetPasswordForm,"enrollPasswordForm");
