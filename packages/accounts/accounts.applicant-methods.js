Meteor.methods({
    "createApplicant": function (data) {
        if (this.userId)
            throw new Meteor.Error('not-logged-out', 'You cannot create applicant while logged in!');

        var Applicants = Seafarers.Collections.Applicants,
        tempPass = 'password',
        userId,
        applicantId,
        hasError;

        try {
            var userData = {
                email: data.email,
                password: tempPass,
                profile: {
                    firstName: data.firstName,
                    lastName: data.lastName,
                    type: 'applicant'
                }
            },
            applicantData = {
                gender: data.gender,
                birthday: data.birthday,
                rank: data.rank,
                vesselType: data.vesselType,
                salary: data.salary,
                contactNo: data.contactNo,
                address: data.address,
                country: data.country,
                state: data.state,
                city: data.city,
                zipCode: data.zipCode
            };

            if (data.lastStep)
                applicantData.lastStep = data.lastStep;

            userId = Accounts.createUser(userData);
            applicantData.userId = userId;
            applicantData = Seafarers.Helpers.Common.trimObject(applicantData);

            applicantId = Applicants.insert(applicantData, function (error) {
                if (error)
                    hasError = true;
            });
        } catch (e) {
            hasError = true;
            throw e;
        } finally {
            if (hasError) {
                if (userId)
                    Meteor.users.remove(userId);
                if (applicantId)
                    Applicants.remove(applicantId);
            }
        }

        // this.unblock();
        //Accounts.sendEnrollmentEmail(userId, data.email);

        return {
            applicantId: applicantId,
            userId: userId,
            tempPass: tempPass
        };
    }
});
