Package.describe({
    name: 'sj-app:accounts',
    version: '0.0.1',
    // Brief, one-line summary of the package.
    summary: '',
});

Package.onUse(function(api) {
    api.versionsFrom('1.1.0.1');

    api.use("accounts-password");
    api.use("meteorhacks:flow-router");
    api.use('tracker');
    api.use('aldeed:simple-schema');
    api.use("sj-app:core");
    api.use("sj-app:core");

    api.addFiles([
        'namespace.js',
    ]);
    
    api.addFiles([
        'accounts.validations.js',
        'accounts.routes.js',
        'accounts.methods.js',
        'accounts.applicant-methods.js',
        'accounts.company-methods.js'
    ]);

    api.addFiles([
        'accounts.on-create-hook.js'
    ], 'server');
});
