Meteor.methods({
    "checkEmail": function (email) {
        return Meteor.users.findOne({emails: {$elemMatch: {address: email}}});
    },
    "getAccountType": function (email) {
    	var user = Meteor.users
    		.findOne({emails: {$elemMatch: {address: email}}});

        return user && user.profile.type;
    }
});