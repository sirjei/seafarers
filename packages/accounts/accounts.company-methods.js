Meteor.methods({
    "createCompany": function (fields) {
        if (this.userId)
            throw new Meteor.Error('not-logged-out', 'You cannot create company while logged in!');

        var Company = Seafarers.Collections.Companies,
        tempPass = 'password',
        userId,
        companyId,
        hasError;

        try {
            var userData = {
                email: fields.email,
                password: tempPass,
                profile: {
                    firstName: fields.firstName,
                    lastName: fields.lastName,
                    type: 'company'
                }
            },
            companyData = {
                companyName: fields.companyName,
                address: fields.address,
                country: fields.country,
                state: fields.state,
                city: fields.city,
                zipCode: fields.zipCode,
                createdAt: new Date()
            };

            userId = Accounts.createUser(userData);
            companyData.userId = [userId];
            companyData = Seafarers.Helpers.Common.trimObject(companyData);

            companyId = Company.insert(companyData, function (error) {
                if (error)
                    hasError = true;
            });
        } catch (e) {
            hasError = true;
            throw e;
        } finally {
            if (hasError) {
                if (userId)
                    Meteor.users.remove(userId);
                if (companyId)
                    Company.remove(companyId);
            }
        }

        // this.unblock();
        //Accounts.sendEnrollmentEmail(userId, fields.email);

        return {
            companyId: companyId,
            userId: userId,
            tempPass: tempPass
        };
    }
});
