var Validator = Seafarers.Core.Factories.Validator,
    CompanySchema = Seafarers.Schemas.Companies,
    SalarySchema = Seafarers.Schemas.Salary;

var resetPasswordForm = Seafarers.Schemas.Users.pick([
  'password',
  'confirmPassword'
]);

var UsersSchema = Seafarers.Schemas.Users.pick([
  'email',
  'firstName',
  'lastName'
]);

var FirstStepForm = new SimpleSchema([UsersSchema, CompanySchema]);

Seafarers.Validations.Accounts.FirstStepForm = new Validator(FirstStepForm,"firstStepForm");
