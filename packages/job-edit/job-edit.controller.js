Seafarers.Controllers.JobEdit = (function (
    schemas, collections
) {
    var self = this,
    stepList = [
        {
            name: 'company-info', displayName: 'Company Information',
            template: 'jobEditCompanyInfo',
            schema: new SimpleSchema([schemas.CreateUser, schemas.DBCompanies,
                schemas.JobOwner]),
            isEnabled: true
        },
        {
            name: 'job-details', displayName: 'Job Details',
            template: 'jobEditJobDetails', schema: schemas.DBJobDetails
        },
        {
            name: 'preview', displayName: 'Preview', template: 'jobEditPreview'
        },
    ],
    currentUser = Meteor.user(),
    userId = currentUser && currentUser._id,
    jobData,
    isValid = true,
    invalidStep,
    schema,
    jobDataClone,
    companies = collections.Companies,
    Steps = new Seafarers.Core.Factories.Steps(stepList, companies,
        function (methods) {
            currentUser = Meteor.user();
            invalidStep = false;

            if (currentUser) {
                userId = currentUser._id;
                profile = currentUser.profile;
                jobData = _.extend(
                    {
                        firstName: profile.firstName,
                        lastName: profile.lastName,
                        email: currentUser.emails[0].address
                    },
                    companies.findOne({userId: userId}),
                    collections.Jobs.findOne({authorId: userId})
                );

                _.find(stepList, function (element, index, list) {
                    if (invalidStep)
                        return true;
                    else {
                        schema = element.schema;

                        if (schema) {
                            jobDataClone = _.clone(jobData);
                            schema.clean(jobDataClone);
                            isValid = schema.newContext()
                                .validate(jobDataClone);

                            // for debugging
                            // var c = schema.newContext();
                            // c.validate(jobDataClone);
                            // console.log(element.name, c.invalidKeys());

                        }

                        if (isValid) {
                            methods.enableNext(element.name);
                            methods.setCompleted(element.name);
                        } else {
                            methods.disableNextSteps(element.name);
                            methods.setIncomplete(element.name);
                            invalidStep = true;
                        }
                    }
                });
            } else
                methods.enableOnly(['company-info']);
        });

    return {
        Steps: Steps,
        subscribe: function (template) {
            template.subscribe('specificCompany', {userId: userId}, {
                onError: function(err) {
                    FlowRouter.go('notFound');
                    throw err;
            }});

            template.subscribe('specificJob', {authorId: userId}, {
                onError: function(err) {
                    FlowRouter.go('notFound');
                    throw err;
            }});
        }
    };
})(Seafarers.Schemas, Seafarers.Collections);
