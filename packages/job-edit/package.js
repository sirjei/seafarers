Package.describe({
    name: 'sj-app:job-edit',
});

Package.onUse(function(api) {
    api.versionsFrom('1.0.5');

    api.use('sj-app:core');
    api.use('sj-app:accounts');
    api.use('underscore');
    api.use('meteorhacks:flow-router');
    api.use('meteorhacks:flow-layout');
    api.use('aldeed:collection2');

    api.addFiles([
        'job-edit.namespace.js',
        'job-edit.controller.js',
        'job-edit.routes.js'
    ], 'client');

    api.addFiles('job-edit.method.js', 'server');
});
