Meteor.methods({
    "createCompanyWithJob": function (data) {
        var jobId,
        userId,
        tempPass,
        caughtError;

        Meteor.call('createCompany', data, function (error, result) {
            if (!error && result) { // if create company succeeded, log in
                userId = result.userId;
                tempPass = result.tempPass;

                jobId = Seafarers.Collections.Jobs.insert({
                    authorId: userId,
                    companyId: result.companyId,
                    state: data.state,
                    country: data.country,
                    companyName: data.companyName,
                    createdAt: new Date(),
                });
            } else
                caughtError = error;
        });

        if (caughtError)
            throw caughtError;
        else
            return {userId: userId, jobId: jobId, tempPass: tempPass};
    }
});
