FlowRouter.route('/jobs/:jobId/edit/:step', {
    name: 'jobEditStep',
    middlewares: [function (path, next) {
        var Steps = Seafarers.Controllers.JobEdit.Steps,
        currentStep = Steps.getOne(FlowRouter.current().params.step),
        currentUser = Meteor.user();

        if (!currentStep)
            next('/notFound');
        else if (
            // if not a company
            (currentUser && currentUser.profile.type !== 'company') ||
            // or not logged in and not on first step
            (currentStep.name !== Steps.getAll()[0].name && !currentUser)
        )
            next('/accessDenied');
        else
            next();
    }],
    action: function (params) {
        var stepTemplate = Seafarers.Controllers.JobEdit.Steps.getOne(params.step).template;

        FlowLayout.render('layoutSteps', {
            mainContent: stepTemplate,
            breadcrumbs: 'jobEditBreadcrumbs',
            stepsProgress: 'jobEditProgress'
        });
    }
});
