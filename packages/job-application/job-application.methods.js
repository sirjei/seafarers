Meteor.methods({
    "addApplicationDraft": function (jobId) {
        var Jobs = Seafarers.Collections.Jobs,
        userId = this.userId;

        Jobs.update(
            {_id: jobId, 'applications.userId': {$ne: userId}},
            {$push: {applications: {userId: userId, 'isSent': false}}},
            function (err) {
                if (err)
                    throw err;
            }
        );

        return true;
    },
    "createApplicantWithDraft": function (data, jobId) {
        var applicantId,
        userId,
        tempPass,
        caughtError;

        Meteor.call('createApplicant', data, function (err, res) {
            if (res) { // if create applicant succeeded, log in
                userId = res.userId;
                tempPass = res.tempPass;
                applicantId = res.applicantId;


                Meteor.call('addApplicationDraft', userId, jobId, function (_err, _res) {
                    if (_err){ // create application draft failed
                        Meteor.users.remove(userId);
                        Seafarers.Collections.Applicants.remove(applicantId);
                        caughtError = _err;
                    }
                });
            } else
                caughtError = err;
        });

        if (caughtError)
            throw caughtError;
        else
            return {userId: userId, tempPass: tempPass, applicantId: applicantId,};
    },
    "updateJobHistory": function (applicantId, jobHistoryId, newData) {
            Seafarers.Collections.Applicants.update({
                _id: applicantId,
                'jobHistories._id': jobHistoryId
            }, {
                $set: newData
            }, function (err, res) {
                if (err)
                    throw err;
            });
    },
    "deleteJobHistory": function (applicantId, jobHistoryId) {
            Seafarers.Collections.Applicants.update(
                applicantId,
                {$pull: {jobHistories: {_id: jobHistoryId}}},
                function (err, affected) {
                    if (err)
                            throw err;
                }
            );
    },
    "sendApplication": function (jobId) {
        var Jobs = Seafarers.Collections.Jobs,
        userId = this.userId,
        firstAttempt, secondAttempt;

        // add an applicationObj with current userId if there's none
        // applicationObj = {userId: userId, isSent: Boolean}
        firstAttempt = Jobs.update(
            {_id: jobId, 'applications.userId': {$ne: userId}},
            {$push: {applications: {userId: userId, isSent: true}}});

        if (!firstAttempt)
            secondAttempt = Jobs.update(
                {_id: jobId, 'applications.userId': userId},
                {$set: {'applications.$.isSent': true}});

        return firstAttempt || secondAttempt;
    }
});
