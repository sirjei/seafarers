Seafarers.Controllers.JobApplication = (function (
    schemas, collections
) {
    var self = this,
    stepList = [
        {
            name: 'basic-info', displayName: 'Basic Information',
            template: 'jobApplicationBasicInfo',
            schema: new SimpleSchema([schemas.CreateUser, schemas.DBBasicInfo]),
            isEnabled: true
        },
        {
            name: 'primary-docs', displayName: 'Primary Documents',
            template: 'jobApplicationPrimaryDocs',
            schema: schemas.DBPrimaryDocs},
        {
            name: 'training-certs', displayName: 'Training Certificates',
            template: 'jobApplicationTrainingCerts',
            schema: schemas.DBTrainingCerts
        },
        {
            name: 'job-history', displayName: 'Job History',
            template: 'jobApplicationJobHistory',
            schema: schemas.DBJobHistories
            },
        {
            name: 'promote', displayName: 'Promote Yourself',
            template: 'jobApplicationPromote'
        },
        {
            name: 'reviewCV', displayName: 'Review your CV',
            template: 'jobApplicationReviewCV'
        }
    ],
    applicants = collections.Applicants,
    isValid = true,
    applicationModalDep = new Tracker.Dependency(),
    Steps = new Seafarers.Core.Factories.Steps(stepList, applicants,
        function (methods) {
            currentUser = Meteor.user();

            if (currentUser) {
                invalidStep = false;
                profile = currentUser.profile;
                applicantData = _.extend(
                    {
                        firstName: profile.firstName,
                        lastName: profile.lastName,
                        email: currentUser.emails[0].address
                    },
                    applicants.findOne({userId: currentUser._id}));

                _.find(stepList, function (element, index, list) {
                    if (invalidStep)
                        return true;
                    else {
                        schema = element.schema;

                        if (schema) {
                            applicantDataClone = _.clone(
                                applicantData);

                            schema.clean(applicantDataClone);

                            isValid = schema.newContext()
                                .validate(applicantDataClone);

                            // for debugging
                            // var c = schema.newContext();
                            // c.validate(applicantDataClone);
                            // console.log(element.name, c.invalidKeys());
                        }

                        if (isValid) {
                            methods.enableNext(element.name);
                            methods.setCompleted(element.name);
                        } else {
                            methods.disableNextSteps(element.name);
                            methods.setIncomplete(element.name);
                            invalidStep = true;
                        }
                    }
                });
            } else
                methods.enableOnly(['basic-info']);
        }),
        applicantSubs, applicantData, invalidStep, schema, applicantDataClone,
        jobId, accType, currentUser, currentTemplate, newTemplate;

    return {
        applicationModal: function (templateName) {
            applicationModalDep.depend();

            currentUser = Meteor.user();

            if (templateName) {
                newTemplate = templateName;
                applicationModalDep.changed();
            } else if (currentUser)
                if (currentUser.profile.type === 'applicant')
                    if (currentUser.emails[0].verified)
                        currentTemplate = 'jobDetailsAfterLogin';
                    else
                        currentTemplate = 'jobDetailsResumeCV';
                else
                    currentTemplate = 'jobDetailsNotAllowed';
            else
                currentTemplate = 'jobDetailsLoginOptions';

            return newTemplate || currentTemplate;
        },
        Steps: Steps,
        subscribeApplicant: function () {
            currentUser = Meteor.user();

            if (currentUser)
                applicantSubs = Meteor.subscribe( // return subscription handle
                    'specificApplicant',
                    {userId: currentUser._id},
                    {onError: function(err) {
                        FlowRouter.go('accessDenied');
                        throw err;
                    }});
        },
        // for all steps except the first one
        initCheck: function (FlowRouter) {
            // check if step is enabled
            if (applicantSubs && applicantSubs.ready()) {
                Steps.check();

                if (!Steps.getOne(FlowRouter.current().params.step).isEnabled)
                    FlowRouter.go('accessDenied');
            }

            this.addApplicationDraft(FlowRouter);
        },
        // create application draft if there's none yet
        addApplicationDraft: function (FlowRouter) {
            if (FlowRouter.subsReady('jobToApply')) {
                jobId = FlowRouter.getParam('jobId');

                if (!collections.Jobs.findOne({
                    _id: jobId,
                    applications: {$elemMatch: {userId: Meteor.userId()}}
                }))
                    Meteor.call('addApplicationDraft', jobId, function (error) {
                        if (error)
                            throw error;
                    });
            }
        }
    };
})(Seafarers.Schemas, Seafarers.Collections);
