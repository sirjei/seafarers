Package.describe({
    name: 'sj-app:job-application',
    version: '0.0.1'
});

Package.onUse(function(api) {
    api.versionsFrom('1.0.5');

    api.use('sj-app:core');
    api.use('reactive-var');
    api.use('underscore');
    api.use('tracker');
    api.use('meteorhacks:flow-router');
    api.use('meteorhacks:flow-layout');
    api.use('aldeed:simple-schema');

    api.addFiles([
      'job-application.namespace.js',
      'job-application.controller.js',
      'job-application.routes.js'
    ], 'client');

    api.addFiles('job-application.methods.js', 'server');
});
