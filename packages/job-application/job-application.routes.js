FlowRouter.route('/jobs/:jobId/apply/:step', {
    name: 'jobApplicationStep',
    middlewares: [function (path, next) {
        var Steps = Seafarers.Controllers.JobApplication.Steps,
        currentStep = Steps.getOne(FlowRouter.current().params.step),
        currentUser = Meteor.user();

        if (!currentStep)
            next('/notFound');
        else if (
            // if not an applicant
            (currentUser && currentUser.profile.type !== 'applicant') ||
            // or not logged in and not on first step
            (currentStep.name !== Steps.getAll()[0].name && !currentUser)
        )
            next('/accessDenied');
        else
            next();
    }],
    subscriptions: function(params) {
        this.register('jobToApply', Meteor.subscribe('specificJob', params.jobId, {
            onError: function(err) {
                FlowRouter.go('notFound');
                throw err;
            }
        }));
    },
    action: function (params) {
        var stepTemplate = Seafarers.Controllers.JobApplication.Steps.getOne(params.step).template;

        FlowLayout.render('layoutSteps', {
            mainContent: stepTemplate,
            breadcrumbs: 'jobApplicationBreadcrumbs',
            stepsProgress: 'jobApplicationProgress'
        });
    }
});
