FlowRouter.route('/jobs', {
    subscriptions: function(params, queryParams) {
        this.register('jobs', Meteor.subscribe('jobs'));
    },
    action: function (params) {
        FlowLayout.render('layoutDefault', {mainContent: 'jobs'});
    }
});

FlowRouter.route('/jobs/:jobId', {
    name: 'jobDetails',
    subscriptions: function(params) {
        this.register('jobToView', Meteor.subscribe('singleJobWithCompany', params.jobId, {
            onError: function(err) {
                if (err)
                    FlowRouter.go('notFound');
            }
        }));
    },
    action: function(params) {
        FlowLayout.render('layoutDefault', {mainContent: 'jobDetails'});
    }
});
