FlowRouter.route('/', {
  action: function(params) {
    FlowLayout.render('layoutDefault');
  }
});

FlowRouter.route('/my-jobs', {
    action: function(params) {
        FlowLayout.render('layoutDefault', {mainContent: ''});
    }
});

FlowRouter.route('/passwords', {
    action: function(params) {
        FlowLayout.render('layoutDefault', {mainContent: 'passwordButtons'});
    }
});

FlowRouter.route('/steps/:step', {
    action: function(params) {
        FlowLayout.render('layoutSteps', {mainContent: params.step});
    }
});


FlowRouter.route('/passwords', {
    action: function(params) {
        FlowLayout.render('layoutDefault', {mainContent: 'passwordButtons'});
    }
});

FlowRouter.notFound = {
    action: function() {
        FlowLayout.render('layoutBlank', {mainContent: 'error404'});
    }
};

// used by chris

FlowRouter.route('/appliedJobs', {
    name: 'appliedJobs',
    action: function(params) {
        FlowLayout.render('layoutDefault', {mainContent: 'appliedJobs'});
    }
});

FlowRouter.route('/accessDenied', {
    name: 'accessDenied',
    action: function (params) {
        FlowLayout.render('accessDenied');
    }
});
